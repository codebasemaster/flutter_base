// import 'dart:ffi';

// import 'dart:ffi';

class Response {
  int? _status;
  dynamic data;
  get status => _status;

  set status(value) => _status = value;
  Response(this.data, this._status);
}
