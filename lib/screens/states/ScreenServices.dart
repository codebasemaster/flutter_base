import 'dart:io';

import 'package:bi_smartie/App.dart';
import 'package:flutter/material.dart';

class ScreenServices extends StatefulWidget {
  const ScreenServices({Key? key}) : super(key: key);

  @override
  State<ScreenServices> createState() => _ScreenServicesState();
}

class _ScreenServicesState extends State<ScreenServices> {
  late double sizeAppBar = 45;

  List<Image> promotions = <Image>[
    Image.asset('assets/images/promotion.png'),
    Image.asset('assets/images/promotion.png'),
    Image.asset('assets/images/promotion.png'),
    Image.asset('assets/images/promotion.png'),
    Image.asset('assets/images/promotion.png'),
    Image.asset('assets/images/promotion.png'),
  ];

  @override
  void initState() {
    if (Platform.isAndroid || Platform.isIOS) {
      sizeAppBar = 45;
    } else {
      sizeAppBar = 10.0;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Container(
        margin: const EdgeInsets.symmetric(horizontal: 14.0, vertical: 5.0),
        child: ClipRRect(
          borderRadius: const BorderRadius.all(Radius.circular(20.0)),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                hspacing,
                Padding(
                  padding: const EdgeInsets.only(bottom: 10.0, top: 30.0),
                  child: Center(
                    child: Text(
                      'BiSmartie Services'.toTitleCase(),
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: colorApp,
                      ),
                    ),
                  ),
                ),
                hspacing,
                ExpandableText(
                  'Exercise your brain with BiSmartie in daily basis to improve brain function and balance your life.',
                  expandText: 'show_more'.tr(),
                  collapseText: 'show_less'.tr(),
                  maxLines: 2,
                  linkColor: Colors.grey.shade500,
                  textAlign: TextAlign.center,
                ),
                hspacing,
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 25.0, vertical: 10),
                  child: boxCard(
                    padding: const EdgeInsets.all(0),
                    margin: const EdgeInsets.all(0),
                    borderRadiusCard: 18.0,
                    child: imgNewsComp,
                  ),
                ),
                hspacing,
                Text(
                  'Check Services from Our Partners',
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    fontSize: h3,
                    fontWeight: FontWeight.w500,
                    color: fontColor,
                  ),
                ),
                hspacing3,
                const Center(
                  child: Text(
                    'We work closely with our partners to bring you joyful experiences and trust like talking to your close friends.',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: p,
                    ),
                  ),
                ),
                hspacing3,
                SizedBox(
                  height: 125,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      boxUnion(
                        Image.asset(
                          'assets/icons/translation.png',
                          fit: BoxFit.cover,
                        ),
                        title: 'Translation',
                        onTap: () {},
                      ),
                      boxUnion(
                        Image.asset(
                          'assets/icons/visa-flight-ticket.png',
                          fit: BoxFit.cover,
                        ),
                        title: 'Visa & Travel',
                        onTap: () {},
                      ),
                    ],
                  ),
                ),
                hspacing,
                Text(
                  'Profit Promotions & Gifts',
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    fontSize: h3,
                    fontWeight: FontWeight.w500,
                    color: fontColor,
                  ),
                ),
                hspacing3,
                const Center(
                  child: Text(
                    'Stay tone with us to profit promotions and gifts from us and our partners.',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: p,
                    ),
                  ),
                ),
                hspacing3,
                ClipRRect(
                  borderRadius: BorderRadius.circular(18.0),
                  child: promotions[0],
                ),
                hspacing,
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: promotions
                      .map((e) => Padding(
                            padding: const EdgeInsets.all(2.0),
                            child: ClipOval(
                              child: Container(
                                  width: 5,
                                  height: 5,
                                  color: colorApp,
                                  child: const SizedBox()),
                            ),
                          ))
                      .toList(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
