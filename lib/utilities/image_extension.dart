import 'dart:io';

import 'package:extended_image/extended_image.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/material.dart';

extension ImageExtension on Image {}

mixin imagePicker implements ImagePicker {
  List<XFile>? imageFileList = [];
  File? imageFile;
  String? downloadURL;

  // Key editorKey = UniqueKey();
  // XFile? photo;

  // Pick an image
  Future<XFile?> pickPicture() async => pickImage(source: ImageSource.gallery);
  // Capture a photo
  Future<XFile?> captureImage() => pickImage(source: ImageSource.camera);
  // Pick a video
  Future<XFile?> pickMovie() => pickVideo(source: ImageSource.gallery);
  // Capture a video
  Future<XFile?> captureMovie() => pickVideo(source: ImageSource.camera);

  void imageFromCamera() async {
    final XFile? pickedFile = await pickImage(source: ImageSource.camera);
    if (pickedFile != null) {
      imageFile = File(pickedFile.path);
    }
  }

  void imageFromGallery() async {
    final XFile? pickedFile = await pickImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      imageFile = File(pickedFile.path);
    }
  }

  void selectImages() async {
    final List<XFile>? selectedImages = await pickMultiImage();
    if (selectedImages!.isNotEmpty) {
      imageFileList!.addAll(selectedImages);
    }
  }

  static Scaffold uploadImage(provider, {File? image}) {
    return Scaffold(
      appBar: null,
      body: Column(
        children: [
          Container(
            margin: const EdgeInsets.only(top: 65),
            height: 60,
            child: Row(
              children: [
                Builder(builder: (context) {
                  return IconButton(
                      onPressed: () {
                        provider.imageFile == null;
                        Navigator.pop(context);
                      },
                      icon: const Icon(Icons.arrow_back_rounded));
                })
              ],
            ),
          ),
          ExtendedImage.file(
            image!,
            fit: BoxFit.contain,
            mode: ExtendedImageMode.editor,
            extendedImageEditorKey: UniqueKey(),
            initEditorConfigHandler: (state) {
              return EditorConfig(
                maxScale: 8.0,
                cropRectPadding: const EdgeInsets.all(20.0),
                hitTestSize: 20.0,
                cropAspectRatio: null,
              );
            },
          ),
        ],
      ),
    );
  }
}
