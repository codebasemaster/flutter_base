import 'package:bi_smartie/https/services/index.dart';

class OrganizationService extends Service {
  String paddyUrl = "/organization";
  OrganizationService() : super(url: "${ENV.apiURL}/organization");

  String createUrl = '/create';
  String updateUrl = '/update';

  Future fetchField() async {
    final response = await get(uri: '/');
    return response;
  }

  void createData(Object data) async => await post(uri: createUrl, body: data);

  void updateData(String id, Object params) async =>
      await post(uri: "$updateUrl/$id", body: params);
}
