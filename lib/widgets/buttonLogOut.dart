import 'dart:convert';
import 'package:bi_smartie/App.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

Widget buttonLogOut() {
  return Builder(
    builder: (context) {
      return ElevatedButton(
        onPressed: () async {
          waitingMessage(context);
          SharedPreferences prefs = await SharedPreferences.getInstance();
          final data = prefs.getString("user");
          if (data != "" || data != null) {
            final decodeRefreshToken = jsonDecode(data!)["refreshToken"];
            AuthController auth = AuthController();
            auth.logOut(decodeRefreshToken).then((value) {
              prefs.setString("user", "");
              ScaffoldMessenger.of(context).hideCurrentSnackBar();
              Navigator.pushReplacementNamed(context, 'login');
              showErrorOrSuccess(text: "auth.logout".tr(), context: context, statusFail: false);
            });
          }
        },
        style: ElevatedButton.styleFrom(
          primary: colorApp,
          padding: const EdgeInsets.symmetric(
            horizontal: 30,
            vertical: 10,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
          ),
        ),
        child: const Text(
          'LogOut',
          style: TextStyle(
            fontSize: 15,
          ),
        ),
      );
    },
  );
}
