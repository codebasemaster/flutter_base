import 'package:bi_smartie/https/models/UserRole.dart';
import 'package:bi_smartie/https/services/index.dart';

class UserRoleService extends Service {
  UserRoleService() : super(url: ENV.apiURL);

  Future<UserRole> getRequest() async {
    final response = await get(uri: '/request_role');
    return response.data.map<UserRole>((value) => value.toJson());
  }
}
