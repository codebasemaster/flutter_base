import 'package:bi_smartie/App.dart';
import 'package:flutter/material.dart';

// no need for a library.
String getDay(int dayNumber) {
  List<String> weekdays = const [
    "Mon",
    "Tue",
    "Wed",
    "Thu",
    "Fri",
    "Sat",
    "Sun",
  ];
  return weekdays[dayNumber];
}

class ThirdRoute extends StatefulWidget {
  @override
  ThirdRoute({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    throw UnimplementedError();
  }
}

class ThirdRouteState extends State<ThirdRoute> {
  List<DateTime> seven = <DateTime>[];
  @override
  void initState() {
    seven = <DateTime>[];
    DateTime start = DateTime(2019, 01, 01);
    DateTime end = DateTime(2019, 01, 08);
    while (start.isBefore(end)) {
      seven.add(start);
      start = start.add(const Duration(days: 1));
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Calendar Page"),
      ),
      body: Column(children: <Widget>[
        Table(
          children: <TableRow>[
            TableRow(
              children: getDaysInWeek(seven),
            ),
            TableRow(
              children: getSevenInWeek(seven),
            ),
          ],
        ),
      ]),
    );
  }
}

List<Widget> getSevenInWeek(List<DateTime> days) {
  return days.map((DateTime time) {
    return Text(
      DateFormat("dd").format(time),
      style: const TextStyle(backgroundColor: Colors.red),
      textAlign: TextAlign.left,
    );
  }).toList();
}

List<Widget> getDaysInWeek(List<DateTime> days) {
  List<Widget> dayWidgets = [];

  days.forEach(
    (DateTime day) {
      // dayWidgets.add(
      // CalendarTile(
      //   isDayOfWeek: true,
      //   dayOfWeek: getDay(day.weekday),
      // ),
      // );
    },
  );

  return dayWidgets;
}
