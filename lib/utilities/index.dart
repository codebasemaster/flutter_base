import 'package:flutter/cupertino.dart';

export './types.dart' show SidebarBuilder;
export './TransitionType.dart' show TransitionType, Transitions;
export './sidebar_base.dart';

export './colors.dart';
export './constraint.dart';
export './helper.dart';

late double radius = 8.0;

late String logoPath = 'assets/images/logo.png';
late String langEn = 'assets/icons/en_language.png';
late String langKh = 'assets/icons/kh_language.png';

final Map<String, double> app_screen = {'sm': 950};
final List<String> images = [
  'assets/images/image-1.jpg',
  'assets/images/image-2.jpg',
  'assets/images/image-3.jpg',
  'assets/images/image-4.jpg',
  'assets/images/image-5.jpg',
  'assets/images/image-6.jpg',
  'assets/images/image-7.jpg',
];

final BorderRadius borderRadius = BorderRadius.circular(radius);
final BorderRadius containerRadius = BorderRadius.all(Radius.circular(radius));
const BoxShadow boxShadow = BoxShadow(
  offset: Offset(0, 2),
  blurRadius: 3,
  spreadRadius: 0.2,
  color: Color.fromARGB(255, 230, 230, 230),
);
const sheetBorder = BorderRadius.only(
    topLeft: Radius.circular(25.0), topRight: Radius.circular(25.0));

late EdgeInsets padding = const EdgeInsets.symmetric(horizontal: 14.0);
late EdgeInsets allPadding = const EdgeInsets.all(14.0);
late SizedBox hspacing = const SizedBox(height: 6.0);
late SizedBox hspacing2 = const SizedBox(height: 8.0);
late SizedBox hspacing3 = const SizedBox(height: 10.0);
late SizedBox wspacing = const SizedBox(width: 14.0);

late Color fontColor = Color.fromARGB(255, 0, 0, 128);
