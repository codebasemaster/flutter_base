import 'package:bi_smartie/utilities/index.dart';
import 'package:flutter/material.dart';

Widget boxUnion(Image image,
    {void Function()? onTap, String? title, bool? isStack = false}) {
  return Builder(builder: (BuildContext context) {
    final double width = MediaQuery.of(context).size.width / 3 - 19.5;
    final double height = (width / 1.5).toDouble();
    return Container(
      height: height + (title != null ? 35 : 0),
      color: isStack == true ? Colors.white : Colors.transparent,
      margin: isStack == true ? const EdgeInsets.all(8.0) : null,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          boxCard(
            onTap: onTap,
            padding: const EdgeInsets.all(0),
            margin: const EdgeInsets.all(0),
            borderRadiusCard: 7.0,
            shadowColor: Colors.grey.shade300,
            elevation: isStack == true ? 0 : 3,
            clip: true,
            child: SizedBox(
              height: height - (isStack == true ? 20 : 0),
              width: MediaQuery.of(context).size.width / 3 -
                  19.5 -
                  (isStack == true ? 20 * 2 : 0),
              child: image,
            ),
          ),
          hspacing,
          title != null
              ? Expanded(
                  child: Text(
                    title.toString(),
                    maxLines: 2,
                    textAlign: TextAlign.center,
                  ),
                )
              : const SizedBox(),
        ],
      ),
    );
  });
}

Widget boxStack(Image image, {void Function()? onTap, String? title}) {
  return Builder(builder: (BuildContext context) {
    final double width = MediaQuery.of(context).size.width / 3 - 19.5;
    return SizedBox(
      child: Stack(
        alignment: AlignmentDirectional.center,
        children: [
          boxCard(
            onTap: onTap,
            padding: const EdgeInsets.all(0),
            margin: const EdgeInsets.all(0),
            borderRadiusCard: 7.0,
            shadowColor: Colors.grey.shade300,
            elevation: 3,
            child: SizedBox(
              height: width / 2,
              width: width,
              child: image,
            ),
          ),
          title != null
              ? Expanded(
                  child: Text(
                    title.toString(),
                    maxLines: 2,
                    textAlign: TextAlign.center,
                  ),
                )
              : const SizedBox(),
        ],
      ),
    );
  });
}

Widget favSchool(Image image, Widget title) {
  return boxCard(
    color: Colors.transparent,
    borderRadiusCard: 7.0,
    shadowColor: Colors.grey.shade300,
    clip: true,
    elevation: 3,
    child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [image, const SizedBox(height: 5.0), title],
      ),
    ),
  );
}
