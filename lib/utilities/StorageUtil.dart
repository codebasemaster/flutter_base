import 'package:shared_preferences/shared_preferences.dart';

class StorageUtil {
  static StorageUtil? _storageUtil;
  static SharedPreferences? _preferences;

  static Future<StorageUtil?> getInstance() async {
    if (_storageUtil == null) {
      // keep local instance till it is fully initialized.
      var secureStorage = StorageUtil._();
      await secureStorage._init();
      _storageUtil = secureStorage;
    }
    return _storageUtil;
  }

  StorageUtil._();
  Future _init() async {
    _preferences = await SharedPreferences.getInstance();
  }

  // get string
  static String getString(String key, {String defValue = ''}) {
    if (_preferences == null) return defValue;
    return _preferences!.getString(key) ?? defValue;
  }

  // put string
  static Future<bool> putString(String key, String value) {
    return _preferences!.setString(key, value);
  }
}

// _refreshToken() {
//   _firebaseMessaging.getToken().then((token) async {
//     print('token: $token');
//     var storageUtil = await StorageUtil.getInstance();
//     storageUtil.putString("token_firebase", token.toString());
//     print('token despues de shared:' + token);
//   }, onError: _tokenRefreshFailure);
// }
