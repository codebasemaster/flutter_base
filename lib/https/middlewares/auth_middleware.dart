import 'dart:convert';
import 'dart:io';

import 'package:bi_smartie/App.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthMiddleware {
  void navigatorNoHistory(BuildContext context, String name) {
    Navigator.pushNamedAndRemoveUntil(context, name, (route) => false);
  }

  void checkAuth(BuildContext context) {
    SharedPreferences.getInstance().then((prefs) {
      final userLocal = prefs.getString("user");
      if (userLocal != "" || userLocal != null) {
        if (userLocal != null) {
          final newUserLocal = jsonDecode(userLocal);
          AuthUser user = AuthUser.fromGenToken(newUserLocal);
          Provider.of<AuthProvider>(context, listen: false).setUserPro(user);
          final userProvider = context.read<AuthProvider>().user;
          if (kDebugMode) {
            print(userProvider?.token);
          }
          if (userProvider?.refreshToken != "") {
            final header = {"authorization": "Basic ${userProvider?.token}"};
            AuthController authController = AuthController();
            authController.verifyCode(userProvider?.token, header).then((value) {
              if (kDebugMode) {
                print(value);
              }
              navigatorNoHistory(context, "sm");
            }).catchError((onError) {
              if (jsonDecode(onError)) {
                if (jsonDecode(onError)["data"]["status"] == 403) {
                  authController.refreshToken(userProvider?.refreshToken).then((value) {
                    final map = jsonDecode(jsonEncode(value["data"]));
                    Provider.of<AuthProvider>(context).setUser(value);
                    navigatorNoHistory(context, "sm");
                  }).catchError((onError) {
                    if (kDebugMode) {
                      print(onError);
                    }
                    prefs.clear();
                  });
                }
              }
            });
          }
        }
      }
    }).catchError((onError) {});
    navigatorNoHistory(context, "login");
  }
}
