import 'package:bi_smartie/App.dart';
import 'package:bi_smartie/screens/parts/NavigationBottom.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class PageEdit extends StatelessWidget {
  PageEdit({Key? key, this.body, this.title, this.appBar}) : super(key: key);
  Widget? body;
  String? title;
  PreferredSizeWidget? appBar;
  final padding = const EdgeInsets.symmetric(horizontal: 14.0);

  @override
  Widget build(BuildContext context) {
    int indexScreen = context.watch<ScreenProvider>().seletedIndex;

    void _onItemTapped(int index) {
      context.read<ScreenProvider>().onItemTapped(index);
      Navigator.pop(context);
    }

    return Scaffold(
      // backgroundColor: Colors.grey.shade100,
      appBar: (appBar == null || title != null)
          ? AppBar(
              centerTitle: true,
              foregroundColor: Theme.of(context).primaryColor,
              title: Text(
                title!.toString(),
                style: TextStyle(color: Theme.of(context).primaryColor),
              ),
              actions: [
                Padding(
                  padding: const EdgeInsets.only(right: 14.0),
                  child: Row(
                    children: [
                      Icon(
                        Icons.notifications,
                        color: Colors.grey.shade400,
                        size: 35,
                      )
                    ],
                  ),
                )
              ],
              backgroundColor: Colors.grey.shade100,
              elevation: 0,
            )
          : appBar,
      body: Container(
        margin: padding,
        padding: const EdgeInsets.only(bottom: 7.0),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(18),
          child: body,
        ),
      ),
      bottomNavigationBar: ClipRRect(
        child: Container(
          decoration: const BoxDecoration(color: Colors.transparent),
          child: NavigationBottom(
            screens: appScreens,
            indexScreen: indexScreen,
            onTap: _onItemTapped,
            selectedColor: colorApp,
            uselessColor: Colors.grey,
          ),
        ),
      ),
    );
  }
}
