import 'dart:io';

class FilePicker {
  String? pickFile(String extension) {
    bool _noExtension = false;
    String _file = "undefined";

    Process.run('which', ['zenity'], runInShell: true).then((pr) {
      if (pr.exitCode != 0) {
        print("zenity not found.");
        return null;
      }

      print("zenity found.");
    });

    if (extension == "undefined") {
      _noExtension = true;
      print("WARNING: extension not specified.");
    }

    Process.run(
            'zenity',
            [
              '--file-selection',
              !_noExtension ? '--file-filter=' + '*.' + extension + '' : '',
            ],
            runInShell: false)
        .then((pr) {
      if (pr.exitCode != 0) {
        print("user canceled choice.");
        print(pr.stderr.toString());
        print(pr.stdout.toString());
        return null;
      }

      _file = pr.stdout.toString();
      print("File: " + _file);
      return _file;
    });

    return null;
  }

  String? pickDirectory() {
    String _dir = "undefined";

    Process.run('which', ['zenity'], runInShell: true).then((pr) {
      if (pr.exitCode != 0) {
        print("zenity not found.");
        return null;
      }

      print("zenity found.");
    });

    Process.run('zenity', ['--file-selection', '--directory'], runInShell: true).then((pr) {
      if (pr.exitCode != 0) {
        print("user canceled choice.");
        print(pr.stderr.toString());
        print(pr.stdout.toString());
        return null;
      }

      _dir = pr.stdout.toString();
      print("Directory: " + _dir);
      return _dir;
    });

    return null;
  }
}
