import 'dart:collection';

import 'package:bi_smartie/App.dart';
import 'package:bi_smartie/screens/parts/Navigation.dart';
import 'package:bi_smartie/screens/parts/NavigationBottom.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppScreen extends StatefulWidget {
  const AppScreen({Key? key}) : super(key: key);

  @override
  State<AppScreen> createState() => _AppScreenState();
}

class _AppScreenState extends State<AppScreen>
    with AutomaticKeepAliveClientMixin<AppScreen> {
  final Color uselessColor = Colors.grey;
  final Color selectedColor = Colors.blue;
  late dynamic screenUser;
  // late Image imageCache;

  final PageStorageBucket _bucket = PageStorageBucket();

  void userLogin() async {
    // await SharePreferences.deleteData('isUserLogin');
    screenUser = await SharePreferences.readData('isUserLogin');
    setState(() {
      UnmodifiableListView(appScreens);
      if (screenUser == true) {
        appScreens[appScreens.length - 1] = const ScreenUser(
          key: PageStorageKey<String>('bi_smartie_ScreeenUser'),
        );
      } else {
        appScreens[appScreens.length - 1] = const PageLogin(
          key: PageStorageKey<String>('bi_smartie_PageLogin'),
        );
      }
    });
  }

  pageSelector(indexScreen) {
    if (indexScreen < appScreens.length) {
      return appScreens[indexScreen];
    }

    return const Scaffold(
      body: Center(
        child: Text(
          "404: No Page Found",
          style: TextStyle(fontSize: 20.0),
        ),
      ),
    );
  }

  @override
  void initState() {
    userLogin();
    super.initState();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  @mustCallSuper
  Widget build(BuildContext context) {
    int indexScreen = context.watch<ScreenProvider>().seletedIndex;
    print(['SizeScreen', MediaQuery.of(context).size]);

    void _onItemTapped(int index) {
      context.read<ScreenProvider>().onItemTapped(index);
    }

    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent, //or set color with: Color(0xFF0000FF)
      statusBarIconBrightness: Brightness.dark, //or set color with: Color(
    ));

    return Scaffold(
      appBar: Navigation() as PreferredSize,
      body: PageStorage(
        bucket: _bucket,
        child: pageSelector(indexScreen),
      ),
      bottomNavigationBar: ClipRRect(
        child: Container(
          decoration: const BoxDecoration(color: Colors.transparent),
          child: NavigationBottom(
            screens: appScreens,
            indexScreen: indexScreen,
            onTap: _onItemTapped,
            selectedColor: colorApp,
            uselessColor: Colors.grey,
          ),
        ),
      ),
    );
  }
}
