import 'package:get_it/get_it.dart' show GetIt;
import 'package:injectable/injectable.dart';
import 'package:bi_smartie/https/controller/index.dart';
import 'package:bi_smartie/https/controller/LocationController.dart';
import 'package:shared_preferences/shared_preferences.dart';

final GetIt locator = GetIt.instance; // GetIt.I

void setup() {
  // locator.registerLazySingleton<ConnectivityService>(() => ConnectivityService());
  locator.registerLazySingleton<AuthController>(() => AuthController());
  locator.registerSingleton<LocationController>(
    LocationController(),
    signalsReady: true,
  );
}

Future<void> _initSharedPref() async {
  final SharedPreferences sharedPref = await SharedPreferences.getInstance();
  locator.registerSingleton<SharedPreferences>(sharedPref);
}

@module
abstract class InjectionModule {
//injecting third party libraries
  @preResolve
  Future<SharedPreferences> get prefs => SharedPreferences.getInstance();
}
