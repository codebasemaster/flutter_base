import 'dart:io';

import 'package:bi_smartie/https/controller/LocationController.dart';
import 'package:bi_smartie/screens/pages/page_university.dart';
import 'package:bi_smartie/screens/pages/page_user.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:bi_smartie/App.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:window_manager/window_manager.dart';
import './injection.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await dotenv.load(fileName: ENV.fileName);
  final currentPlaform = defaultTargetPlatform;
  if (Platform.isWindows || Platform.isLinux || Platform.isMacOS) {
    WindowManager.instance.setMinimumSize(const Size(425.0, 972.0));
    WindowManager.instance.setMaximumSize(const Size(425.0, 972.0));
  }
  await EasyLocalization.ensureInitialized();
  setup();
  runApp(
    RestartWidget(
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => ThemeProvider()),
          ChangeNotifierProvider(create: (_) => ScreenProvider()),
          ChangeNotifierProvider(create: (_) => LocationProvider()),
          ChangeNotifierProvider(create: (_) => AuthProvider()),
        ],
        child: EasyLocalization(
          supportedLocales: const [
            Locale('en', 'US'),
            Locale('km', 'KH'),
          ],
          path:
              'assets/translations', // <-- change the path of the translation files
          fallbackLocale: const Locale('en', 'US'),
          child: const ScreenApp(),
        ),
      ),
    ),
  );
}

class ScreenApp extends StatelessWidget {
  const ScreenApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(statusBarColor: Colors.transparent),
    );
    locator.get<LocationController>().fetchData();
    // context.watch<LocationProvider>().fetchData();
    // context.watch<AuthController>().register();
    context.setLocale(const Locale('en', 'US'));

    return MediaQuery(
      data: const MediaQueryData(),
      child: MaterialApp(
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        locale: context.locale,
        debugShowCheckedModeBanner: false,
        theme: context.watch<ThemeProvider>().getTheme(),
        home: Builder(
          builder: (context) => const SplashContent(),
        ),
        builder: EasyLoading.init(),
        routes: <String, WidgetBuilder>{
          'init_screen': (BuildContext context) => const SplashContent(),
          'sm': (BuildContext context) => const AppScreen(),
          'login': (BuildContext context) => const PageLogin(),
          'dev': (BuildContext context) => const PageUniversity()
        },
      ),
    );
  }
}

// title: 'Localizations Sample App',
// localizationsDelegates: [
//   AppLocalizations.delegate, // Add this line
//   GlobalMaterialLocalizations.delegate,
//   GlobalWidgetsLocalizations.delegate,
//   GlobalCupertinoLocalizations.delegate,
// ],
// supportedLocales: [
//   Locale('en', ''), // English, no country code
//   Locale('es', ''), // Spanish, no country code
// ],
