import 'package:bi_smartie/utilities/asset_cache/asset_cache.dart';
import 'package:flutter/material.dart';

final stringAssets = TextAssetCache(basePath: 'assets/strings/');
final stringAssetsWithoutBasePath = TextAssetCache();
final jsonAssets = JsonAssetCache(basePath: 'assets/jsons/');
final imageAssets = ImageAssetCache(basePath: 'assets/images/');
final iconAssets = ImageAssetCache(basePath: 'assets/icons/');
final byteAssets = ByteDataAssetCache(basePath: 'assets/images/');

// login
late Image imgLoginPng = Image.asset('assets/images/image_login.png', fit: BoxFit.cover);
late Image imgLogin = Image.asset('assets/images/ogin.jpg', fit: BoxFit.cover);

// compus
late Image imgScholarshipComp =
    Image.asset('assets/images/scholarship_comp.jpg', fit: BoxFit.cover);
late Image imgForumComp = Image.asset('assets/images/forum_comp.jpg', fit: BoxFit.cover);
late Image imgSchoolComp = Image.asset('assets/images/school_comp.jpg', fit: BoxFit.cover);
late Image imgNewsComp = Image.asset('assets/images/news_comp.jpg', fit: BoxFit.cover);
late Image imghealthComp = Image.asset('assets/images/mentalhealth_comp.jpg', fit: BoxFit.cover);

// logo
late Image imgLogoPng = Image.asset('assets/images/logo.png', fit: BoxFit.cover);
