import 'dart:convert';
import 'package:bi_smartie/App.dart';
import 'package:bi_smartie/https/controller/Controller.dart';
import 'package:bi_smartie/https/models/Model.dart';

class LocationController extends GetItAppModel {
  Map<String, dynamic> _location = {};
  List _province = []; //New
  List _district = []; //New
  List _commune = []; //New
  List _village = []; //New

  late Map<String, String> _locationDisplay = {};

  final Map<String, dynamic> _selectedLocation = {
    "provinceId": null,
    "districtId": null,
    "communeId": null,
    "villageId": null
  };

  LocationController() {
    Future.delayed(const Duration(seconds: 3)).then((_) => getIt.signalReady(this));
  }

  Map<String, dynamic> get location => _location;
  Map<String, dynamic> get selectedLocation => _selectedLocation;
  Map<String, String> get displayName => _locationDisplay;

  List get province => _province;
  List get district => _district;
  List get commune => _commune;
  List get village => _village;

  @override
  Future<void> fetchData() async {
    try {
      _location = await jsonAssets.load('contries.json');
      _province = _location['provinces'].toList();
      notifyListeners();
    } on Exception catch (e) {
      print("Unable to LocationProvider $e");
    }
  }

  Future<void> getDistrict(int provinceId) async {
    try {
      _selectedLocation['provinceId'] = provinceId;
      _locationDisplay = {};
      _locationDisplay['province'] = _location['provinces']
          .firstWhere((value) => value['id'] == provinceId, orElse: () => null)['name_kh'];
      _district = await _location['districts']
          .where((value) => value['province_id'] == provinceId)
          .toList();
      notifyListeners();
    } on Exception catch (e) {
      print("Unable to LocationProvider $e");
    }
  }

  Future<void> getCommune(int districtId) async {
    try {
      _selectedLocation['districtId'] = districtId;
      _locationDisplay['district'] = _location['districts']
          .firstWhere((value) => value['id'] == districtId, orElse: () => null)['name_kh'];
      _commune =
          await _location['communes'].where((value) => value['district_id'] == districtId).toList();

      _locationDisplay.remove('commune');
      _locationDisplay.remove('village');
      notifyListeners();
    } on Exception catch (e) {
      print("Unable to LocationProvider $e");
    }
  }

  Future<void> getVillage(int communeId) async {
    try {
      _selectedLocation['communeId'] = communeId;
      _locationDisplay['commune'] = _location['communes']
          .firstWhere((value) => value['id'] == communeId, orElse: () => null)['name_kh'];
      _village =
          await _location['villages'].where((value) => value['commune_id'] == communeId).toList();
      _locationDisplay.remove('village');
      notifyListeners();
    } on Exception catch (e) {
      print("Unable to LocationProvider $e");
    }
  }

  Future<void> getNameVillage(int villageId) async {
    try {
      _locationDisplay['village'] = _location['villages']
          .firstWhere((value) => value['id'] == villageId, orElse: () => null)['name_kh'];
      notifyListeners();
    } on Exception catch (e) {
      print("Unable to LocationProvider $e");
    }
  }
}
