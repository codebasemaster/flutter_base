import 'package:image_picker/image_picker.dart';

class Image extends ImagePicker {
  List<XFile>? imageFileList = [];
  // XFile? photo;

  // // Pick an image
  // final XFile? image = await pickImage(source: ImageSource.gallery);
  // // Capture a photo
  // final XFile? photo = await pickImage(source: ImageSource.camera);
  // // Pick a video
  // final XFile? pick_image = await pickVideo(source: ImageSource.gallery);
  // // Capture a video
  // final XFile? video = await pickVideo(source: ImageSource.camera);

  void selectImages() async {
    final List<XFile>? selectedImages = await pickMultiImage();
    if (selectedImages!.isNotEmpty) {
      imageFileList!.addAll(selectedImages);
    }
  }

  void openCamera() async {
    final XFile? photo = await pickImage(source: ImageSource.camera);
    // photo.saveTo('/assets/profile.jpg');
  }
}
