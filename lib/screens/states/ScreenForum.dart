import 'dart:io';

import 'package:bi_smartie/App.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ScreenForum extends StatefulWidget {
  const ScreenForum({Key? key}) : super(key: key);

  @override
  State<ScreenForum> createState() => _ScreenForumState();
}

class _ScreenForumState extends State<ScreenForum> {
  late double sizeAppBar = 45;
  @override
  void initState() {
    if (Platform.isAndroid || Platform.isIOS) {
      sizeAppBar = 45;
    } else {
      sizeAppBar = 10.0;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Size sizeScreen = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Container(
        margin: const EdgeInsets.symmetric(horizontal: 14.0, vertical: 5.0),
        child: Column(
          children: [
            hspacing,
            Container(
              padding: const EdgeInsets.only(bottom: 10.0, top: 30.0),
              child: Text(
                'Discussion Forum'.toTitleCase(),
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                  color: colorApp,
                ),
              ),
            ),
            hspacing,
            ExpandableText(
              'A home to meet experts, seniors, juniors, friends and friend-tobe to share, discuss and learn from each others.',
              expandText: 'show_more'.tr(),
              collapseText: 'show_less'.tr(),
              maxLines: 2,
              linkColor: Colors.grey.shade500,
              textAlign: TextAlign.center,
            ),
            hspacing,
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 25.0, vertical: 10),
              child: boxCard(
                padding: const EdgeInsets.all(0),
                margin: const EdgeInsets.all(0),
                borderRadiusCard: 18.0,
                child: imgForumComp,
              ),
            ),
            const Expanded(child: Center(child: Text('Comming soon.....')))
          ],
        ),
      ),
    );
  }
}
