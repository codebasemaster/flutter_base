import 'package:flutter/material.dart';

class NotificationBadge extends StatelessWidget {
  final int totalNotifications;
  final double? width;
  final double? height;

  const NotificationBadge({super.key, required this.totalNotifications, this.width, this.height});

  @override
  Widget build(BuildContext context) {
    return ClipOval(
      child: Container(
        width: width ?? 15.0,
        height: height ?? 15.0,
        color: Colors.red,
        child: FittedBox(
          alignment: Alignment.center,
          fit: BoxFit.contain,
          child: Text(
            '$totalNotifications',
            style: const TextStyle(color: Colors.white, fontSize: 10),
          ),
        ),
      ),
    );
  }
}
