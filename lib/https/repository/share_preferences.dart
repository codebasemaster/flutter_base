import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class SharePreferences {
  static void saveData(String key, dynamic value) async {
    final prefs = await SharedPreferences.getInstance();
    if (value == null) {
      return;
    }
    if (value is int) {
      prefs.setInt(key, value);
    } else if (value is bool) {
      prefs.setBool(key, value);
    } else if (value is String) {
      prefs.setString(key, value);
    } else {
      prefs.setString(key, jsonEncode(value));
    }
    // print("Invalid Type of SharePreferences");
  }

  static readData(String key) async {
    final prefs = await SharedPreferences.getInstance();
    final obj = prefs.get(key);
    if (obj is int) {
      return obj;
    } else if (obj is Object) {
      return obj;
    }
    return obj;
  }

  static Future<bool> deleteData(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.remove(key);
  }
}
