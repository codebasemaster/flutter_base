import 'package:community_material_icon/community_material_icon.dart';
import 'package:control_style/decorated_input_border.dart';
import 'package:bi_smartie/widgets/auth/route_auth_animate_widget.dart';
import 'package:flutter/material.dart';

import '../../App.dart';

bool? validateUserName(String? userValue) {
  String pattern = r'^[a-zA-Z]*$';
  RegExp regex = RegExp(pattern);
  return regex.hasMatch(userValue ?? "");
}

bool? validateEmail(String? emailValue) {
  String pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regex = RegExp(pattern);
  return regex.hasMatch(emailValue ?? "");
}

bool? validateUsername(String? username) => RegExp(r'(^[a-zA-Z0-9]*$)').hasMatch(username ?? "");
bool? validatePassword(String? passwordValue) => (passwordValue?.length ?? 0) >= 6;

bool? validatePhoneNumber(String? phoneValue) => phoneValue?.isEmpty ?? true
    ? false
    : RegExp(r'^[+855|0|+8550][-.\s]?\(?\d{1,3}?\)?[-.\s]?\d{1,4}[-.\s]?\d{1,4}[-.\s]?\d{1,9}$')
        .hasMatch(phoneValue ?? "");
DecoratedInputBorder errorBorder() {
  return DecoratedInputBorder(
    child: OutlineInputBorder(
      borderSide: const BorderSide(width: 3, color: Colors.redAccent), //<-- SEE HERE
      borderRadius: BorderRadius.circular(10),
    ),
    shadow: const [boxShadow],
  );
}

DecoratedInputBorder borderShadowWhite() {
  return DecoratedInputBorder(
    child: OutlineInputBorder(
      borderSide: const BorderSide(width: 3, color: Colors.white), //<-- SEE HERE
      borderRadius: BorderRadius.circular(10),
    ),
    shadow: const [boxShadow],
  );
}

InputDecoration decorationOtp(String hintText) {
  return InputDecoration(
    hintText: hintText,
    hintStyle: const TextStyle(
      color: colorHintText,
      fontSize: 20,
    ),
    counterText: '',
    filled: true,
    focusColor: colorApp,
    fillColor: Colors.white,
    hoverColor: Colors.white,
    enabledBorder: OutlineInputBorder(
      borderSide: const BorderSide(width: 3, color: Colors.white), //<-- SEE HERE
      borderRadius: BorderRadius.circular(10),
    ),
    contentPadding: const EdgeInsets.only(bottom: 8),
  );
}

InputDecoration decorationInputForm({
  String? label,
  required FocusNode focusNode,
  dynamic iconForm,
  bool? isHidden,
  IconData? subfix,
  String? hintText,
  bool? isDense,
  void Function()? setState,
  bool? notShadow,
}) {
  final shadowOrNot = notShadow == true ? InputBorder.none : borderShadowWhite();
  return InputDecoration(
    errorStyle: const TextStyle(
      color: Colors.redAccent,
      letterSpacing: 0,
    ),
    isDense: isDense,
    focusedErrorBorder: errorBorder(),
    errorBorder: errorBorder(),
    label: label != null
        ? Text(
            label.tr(),
            style: const TextStyle(
              fontSize: 15,
            ),
          )
        : null,
    hintText: hintText,
    hintStyle: const TextStyle(
      color: colorHintText,
      fontSize: 15,
    ),
    hoverColor: Colors.white,
    prefixIcon: iconForm != null
        ? Padding(
            padding: const EdgeInsets.all(7),
            child: Container(
              width: 20,
              height: 20,
              child: iconForm is IconData
                  ? Icon(
                      iconForm,
                      size: 20,
                      color: colorApp,
                    )
                  : iconForm,
            ),
          )
        : null,
    border: shadowOrNot,
    labelStyle: TextStyle(
      color: focusNode.hasFocus ? Colors.grey : colorHintText,
    ),
    enabledBorder: shadowOrNot,
    fillColor: Colors.white,
    filled: true,
    focusColor: Colors.grey,
    focusedBorder: shadowOrNot,
    contentPadding:
        iconForm != null ? const EdgeInsets.symmetric(vertical: 6) : const EdgeInsets.all(8.0),
    suffixIcon: subfix == null
        ? ((isHidden != null)
            ? (isHidden == true
                ? iconButtonWidget(
                    icon: const Icon(
                      Icons.visibility,
                      color: colorApp,
                    ),
                    onPressed: setState,
                  )
                : iconButtonWidget(
                    onPressed: setState,
                    icon: Icon(
                      Icons.visibility_off,
                      color: focusNode.hasFocus ? colorApp : colorHintText,
                    ),
                  ))
            : null)
        : iconButtonWidget(
            onPressed: () {},
            icon: Icon(
              subfix,
              color: colorApp,
            ),
          ),
  );
}

IconButton iconButtonWidget({
  required void Function()? onPressed,
  required Widget icon,
}) {
  return IconButton(
    onPressed: onPressed,
    icon: icon,
    color: Colors.white,
    hoverColor: Colors.white,
    splashColor: Colors.white,
    focusColor: Colors.white,
    highlightColor: Colors.white,
  );
}

Widget formFieldInputContainer(
    BuildContext context, Widget Function(BuildContext context) anyWidget) {
  return anyWidget(context);
}

InkWell linkInLogin(BuildContext context, String title, String nameRoute,
        {void Function()? onTab}) =>
    InkWell(
      focusColor: Colors.white,
      hoverColor: Colors.white,
      highlightColor: Colors.white,
      splashColor: Colors.white,
      onTap: onTab ??
          () {
            Navigator.of(context).push(
              CustomRouterPage(
                direction: AxisDirection.up,
                child: nameRoute == "register" ? const PageRegister() : const PageLogin(),
              ),
            );
          },
      child: Text(
        title,
        style: const TextStyle(
          color: colorApp,
          fontSize: 13,
        ),
      ),
    );
InkWell sendOTP({void Function()? onTap, String? title, Color? colorData}) => InkWell(
      focusColor: Colors.white,
      hoverColor: Colors.white,
      highlightColor: Colors.white,
      splashColor: Colors.white,
      onTap: onTap,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            title!,
            style: TextStyle(
              color: colorData,
            ),
            textAlign: TextAlign.center,
          ),
          Icon(
            CommunityMaterialIcons.send_outline,
            color: colorData,
          )
        ],
      ),
    );
