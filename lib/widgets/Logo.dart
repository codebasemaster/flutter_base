import 'package:flutter/material.dart';

class AppLogo extends StatelessWidget {
  const AppLogo({Key? key}) : super(key: key);
  final String logoPath = 'assets/images/splashscreen.png';
  // final String logoPath = 'assets/images/logo.png';

  @override
  Widget build(BuildContext context) {
    // return Image(image: AssetImage(logoPath));
    return Image.asset(logoPath, fit: BoxFit.fill);
  }
}
