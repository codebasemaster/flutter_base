import 'package:flutter/material.dart';

Widget BTNSheet({Key? key, required Widget label, Widget? body}) {
  return Builder(
    builder: (context) {
      return Center(
        child: ElevatedButton(
          child: label,
          onPressed: () {
            showModalBottomSheet(
              isScrollControlled: true,
              // backgroundColor: Colors.lightBlueAccent,
              // barrierColor: Colors.black.withOpacity(0.3),
              barrierColor: Colors.white10,
              // constraints: BoxConstraints.expand(),
              context: context,
              builder: (context) {
                return Wrap(
                  spacing: 5.0,
                  children: const [
                    ListTile(
                      leading: Icon(Icons.share),
                      title: Text('Share'),
                    ),
                    ListTile(
                      leading: Icon(Icons.copy),
                      title: Text('Copy Link'),
                    ),
                    ListTile(
                      leading: Icon(Icons.edit),
                      title: Text('Edit'),
                    ),
                  ],
                );
              },
            );
          },
        ),
      );
    },
  );
}
