import 'package:bi_smartie/https/controller/LocationController.dart';
import 'package:bi_smartie/injection.dart';
import 'package:bi_smartie/widgets/DropdownMenu.dart';
import 'package:flutter/material.dart';

class Place extends StatefulWidget {
  Place({Key? key, this.commune_id, this.district_id, this.province_id}) : super(key: key);
  int? province_id;
  int? district_id;
  int? commune_id;
  int? village_id;

  @override
  State<Place> createState() => _PlaceState();
}

class _PlaceState extends State<Place> {
  late double padding = 10.0;
  late double divide = 1.0;
  late double resize = 40;

  @override
  Widget build(BuildContext context) {
    List<dynamic> province = locator<LocationController>().province.toList();
    Map<String, dynamic> selectedLocation = locator<LocationController>().selectedLocation;
    List<dynamic> district = [];
    List<dynamic> commune = [];
    List<dynamic> village = [];
    void getDistrict(int provinceId) => locator.get<LocationController>().getDistrict(provinceId);

    void getCommune(int districtId) => locator.get<LocationController>().getCommune(districtId);

    void getVillage(int communeId) => locator.get<LocationController>().getVillage(communeId);

    late Size size_screen = MediaQuery.of(context).size;
    return Container(
      alignment: Alignment.topLeft,
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        const SizedBox(height: 25, child: Text('ទីតាំង')),
        Row(
          children: [
            SizedBox(
              width: (size_screen.width - resize) / divide,
              child: DropdownMenu(
                selecteValue: widget.province_id,
                hint: const Text('ខេត្ត'),
                items: province,
                itemValue: 'id',
                displayName: 'name_kh',
                onChanged: (value) {
                  setState(() {
                    widget.province_id = value;
                    widget.district_id = null;
                    district = [];
                    divide = 2.0;
                  });
                  getDistrict(value);
                },
              ),
            ),
            if (widget.province_id != null && district.isNotEmpty)
              SizedBox(
                width: (size_screen.width - resize) / divide,
                child: DropdownMenu(
                  hint: const Text('ស្រុក'),
                  items: district,
                  itemValue: 'id',
                  displayName: 'name_kh',
                  selecteValue: widget.district_id,
                  onChanged: (value) {
                    setState(() {
                      widget.district_id = value;
                      widget.commune_id = null;
                      commune = [];
                    });
                    getCommune(value);
                  },
                ),
              ),
          ],
        ),
        Row(
          children: [
            if (widget.district_id != null && commune.isNotEmpty)
              SizedBox(
                width: (size_screen.width - resize) / divide,
                child: DropdownMenu(
                  selecteValue: widget.commune_id,
                  hint: const Text('ភូមិ'),
                  items: commune,
                  itemValue: 'id',
                  displayName: 'name_kh',
                  onChanged: (value) {
                    setState(() {
                      widget.commune_id = value;
                      widget.village_id = null;
                      village = [];
                    });
                    getVillage(value);
                  },
                ),
              ),
            if (widget.commune_id != null && village.isNotEmpty)
              SizedBox(
                width: (size_screen.width - resize) / divide,
                child: DropdownMenu(
                  selecteValue: widget.village_id,
                  hint: const Text('ឃំុ'),
                  items: village,
                  itemValue: 'id',
                  displayName: 'name_kh',
                  onChanged: (value) {
                    setState(() {
                      widget.village_id = value;
                    });
                  },
                ),
              ),
          ],
        )
      ]),
    );
  }
}
