import 'package:flutter/material.dart';

class InputPhone extends StatefulWidget {
  const InputPhone({Key? key}) : super(key: key);

  @override
  _InputPhoneState createState() => _InputPhoneState();
}

class _InputPhoneState extends State<InputPhone> {
  final List<String> _items = ['+855'];
  final TextEditingController _phoneNumberController = TextEditingController();
  String? _value;

  @override
  void initState() {
    super.initState();
    _value = _items.first;
  }

  void _onDropDownChanged(String? value) {
    setState(() {
      _value = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 32),
        height: 56,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(28),
          color: Colors.grey[200],
        ),
        child: Row(
          children: <Widget>[
            DropdownButtonHideUnderline(
              child: Container(
                padding: const EdgeInsets.fromLTRB(32, 8, 16, 8),
                child: DropdownButton<String>(
                  onChanged: _onDropDownChanged,
                  value: _value,
                  items: _items.map((value) {
                    return DropdownMenuItem<String>(value: value, child: Text(value));
                  }).toList(),
                ),
              ),
            ),
            Container(width: 1, color: Colors.grey[300]),
            Expanded(
              child: TextFormField(
                keyboardType: TextInputType.phone,
                autofocus: false,
                controller: _phoneNumberController,
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.fromLTRB(16, 16, 8, 16),
                  border: InputBorder.none,
                  suffixIcon: Padding(
                    padding: const EdgeInsets.only(right: 16),
                    child: Icon(Icons.cancel, color: Colors.grey[400]),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
