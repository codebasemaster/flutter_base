import 'package:flutter_dotenv/flutter_dotenv.dart';

class ENV {
  static String get fileName => ".env";
  static String get apiURL => dotenv.env["API_URL"] ?? "";
  //API_LOCATION
  static String get locationUrl => dotenv.env["API_LOCATION"] ?? "";
}
