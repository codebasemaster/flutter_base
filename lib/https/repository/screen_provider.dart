import 'share_preferences.dart';
import 'package:flutter/material.dart';

class ScreenProvider extends ChangeNotifier {
  int _selectedIndex = 0; //New
  Size _screen = Size.zero;
  bool? _verifyCodeStatus;
  bool _isToPageLogin = false;
  Size get screen => _screen;
  bool? get verifyCodeStatus => _verifyCodeStatus;
  int get seletedIndex => _selectedIndex;
  bool get isToPageLogin => _isToPageLogin;

  late Brightness _brightness = Brightness.dark;

  Brightness get statusColor => _brightness;

  void toPageLogin() {
    _isToPageLogin = true;
    notifyListeners();
  }

  void isBright(bool? light) {
    _brightness = light == true ? Brightness.light : Brightness.dark;
    notifyListeners();
  }

  void setVerifyCode(bool code) {
    _verifyCodeStatus = code;
    notifyListeners();
  }
  // void setLocationLization({String data,String? code}) {

  // }
  void onItemTapped(index) {
    _selectedIndex = index;
    notifyListeners();
  }

  Future<void> onScreenChanged(Size screen) async {
    _screen = screen;
    SharePreferences.saveData('screen', screen);
    notifyListeners();
  }
}
