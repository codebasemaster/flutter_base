import 'dart:convert';
import 'package:bi_smartie/widgets/index.dart';
import 'package:flutter/material.dart';

class LocationProvider extends ChangeNotifier {
  late Map<String, dynamic> _location;
  late List _province = []; //New
  late List _district = []; //New
  late List _commune = []; //New
  late List _village = []; //New

  Map<String, dynamic> get location => _location;

  List get province => _province;
  List get district => _district;
  List get commune => _commune;
  List get village => _village;

  Future<void> fetchData() async {
    try {
      _location = await json.decode(await Helper.fetchLocal('datas/contries.json'));
      _province = _location['provinces'].toList();
      notifyListeners();
    } on Exception catch (e) {
      print("Unable to LocationProvider $e");
    }
  }

  Future<void> onProvinceChanged(int provinceId) async {
    try {
      _district = await _location['districts']
          .where((value) => value['province_id'] == provinceId)
          .toList();
    } on Exception catch (e) {
      print("Unable to LocationProvider $e");
    }
    notifyListeners();
  }

  Future<void> onDistrictChanged(int districtId) async {
    try {
      _commune =
          await _location['communes'].where((value) => value['district_id'] == districtId).toList();
    } on Exception catch (e) {
      print("Unable to LocationProvider $e");
    }
    notifyListeners();
  }

  Future<void> onCommuneChanged(int communeId) async {
    try {
      _village =
          await _location['villages'].where((value) => value['village_id'] == communeId).toList();
      notifyListeners();
    } on Exception catch (e) {
      print("Unable to LocationProvider $e");
    }
  }
}
