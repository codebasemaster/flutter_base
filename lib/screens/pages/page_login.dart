import 'dart:convert';

import 'package:bi_smartie/App.dart';
import 'package:bi_smartie/https/models/auth/user_ui.dart';
import 'package:bi_smartie/screens/pages/page_register.dart';
import 'package:bi_smartie/screens/pages/page_user.dart';
import 'package:bi_smartie/utilities/auth/base_usage.dart';
import 'package:bi_smartie/widgets/auth/base_auth_field_widget.dart';
import 'package:bi_smartie/widgets/auth/route_auth_animate_widget.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class PageLogin extends StatefulWidget {
  const PageLogin({Key? key}) : super(key: key);

  @override
  State<PageLogin> createState() => _PageLoginState();
}

class _PageLoginState extends State<PageLogin> {
  final keyController = TextEditingController();
  final passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  var _statusPhone = false;

  final currentPlaform = defaultTargetPlatform;
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size sizeScreen = MediaQuery.of(context).size;
    final bool isRequestLogin = context.watch<ScreenProvider>().isToPageLogin;
    final subTitle = "auth.login.sub_title".tr();
    const loginKey = "auth.login.key";
    const passwordKey = "auth.password";
    final textForgetPassword = '${"auth.login.forget_password".tr()} ?';

    void onPressed() {
      SharePreferences.saveData('isUserLogin', true);
      RestartWidget.restartApp(context);
      // Navigator.push(context, CustomRouterPage(child: const PageUser()));

      // context.read<ScreenProvider>().setVerifyCode(false);
      // if (_formKey.currentState!.validate()) {
      //   if (_statusPhone == false) {
      //     AuthController auth = AuthController();
      //     waitingMessage(context);
      //     auth.getLocation().then((value) async {
      //       AuthUser user = AuthUser();
      //       user.displayName = keyController.text;
      //       user.password = passwordController.text;
      //       final authLogin = AuthController();

      //       authLogin.logIn(user).then((value) async {
      //         final map = jsonDecode(jsonEncode(value["data"]));
      //         Provider.of<AuthProvider>(context, listen: false).setUser(map);
      //         ScaffoldMessenger.of(context).hideCurrentSnackBar();
      //         RestartWidget.restartApp(context);
      //       }).catchError((onError) {
      //         final status = jsonDecode(onError)["status"];
      //         if (kDebugMode) {
      //           print(jsonDecode(onError));
      //         }
      //         if (status == 400) {
      //           showErrorOrSuccess(
      //               text: "auth.error.email_error.match".tr(), context: context, statusFail: true);
      //         } else if (status == 403) {
      //           showErrorOrSuccess(
      //             text: "auth.error.password_error.match".tr(),
      //             context: context,
      //             statusFail: true,
      //           );
      //         }
      //       });
      //     }).catchError((onError) {
      //       if (kDebugMode) {
      //         print(['catchError', onError]);
      //       }
      //     });
      //   } else {
      //     Navigator.of(context).push(
      //       CustomRouterPage(
      //         direction: AxisDirection.up,
      //         // child: PageVerifyCode(phoneNumber: keyController.text),
      //         child: const Text('hello world'),
      //       ),
      //     );
      //   }
      // }
    }

    String? onChangeUserOrPhone(String? value) {
      if (currentPlaform != TargetPlatform.linux &&
          currentPlaform != TargetPlatform.windows) {
        if (validatePhoneNumber(value) == true) {
          setState(() {
            _statusPhone = true;
          });
        } else {
          setState(() {
            _statusPhone = false;
          });
        }
      }
      return null;
    }

    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 25),
                  child: ClipRRect(
                    borderRadius: borderRadius,
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Image.asset('assets/images/image_login.png'),
                        const Text(
                          'Login',
                          style: TextStyle(
                              fontSize: 24, fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 5.0),
                      child: Text(
                        'Please login to manage your preferences and access to more features.',
                        textAlign: TextAlign.center,
                      ),
                    ),
                    const SizedBox(height: 25),
                    Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          BaseAuthFieldWidget(
                            controller: keyController,
                            screen: sizeScreen,
                            prefixicon:
                                Image.asset('assets/icons/kh_language.png'),
                            label: loginKey,
                            subfixicon: null,
                            secureText: false,
                            onChanged: onChangeUserOrPhone,
                            validator: (value) {
                              if (value?.isEmpty ?? true) {
                                return "auth.error.phone_error.require".tr();
                              }
                              return null;
                            },
                          ),
                          Visibility(
                            visible:
                                true, // _statusPhone == false ? true : false,
                            child: Column(
                              children: [
                                const SizedBox(height: 20),
                                BaseAuthFieldWidget(
                                  controller: passwordController,
                                  screen: sizeScreen,
                                  label: passwordKey,
                                  // prefixicon: CommunityMaterialIcons.lock_outline,
                                  subfixicon: false,
                                  secureText: true,
                                  keyBoardType: TextInputType.visiblePassword,
                                  validator: (value) =>
                                      responseValidatePassword(value),
                                  maxLines: 1,
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(height: 10),
                          Align(
                            alignment: Alignment.centerRight,
                            child: linkInLogin(
                              context,
                              textForgetPassword,
                              "register",
                              onTab: () {
                                context.read<ScreenProvider>().toPageLogin();
                                Navigator.of(context).push(CustomRouterPage(
                                  direction: AxisDirection.up,
                                  child: const PageRegister(),
                                ));
                              },
                            ),
                          ),
                          const SizedBox(height: 20),
                          elevateButtonAuth(onPressed, sizeScreen, subTitle),
                          const SizedBox(height: 20),
                          linkInLogin(
                              context, 'Sign Up for New Account', "register",
                              onTab: () {
                            context.read<ScreenProvider>().toPageLogin();
                            Navigator.of(context).push(CustomRouterPage(
                              direction: AxisDirection.up,
                              child: const PageRegister(),
                            ));
                          }),
                        ],
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Column(
                        children: [
                          const SizedBox(height: 25),
                          const Text('----- Login via -----'),
                          const SizedBox(height: 25),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ClipOval(
                                child: Image.asset(
                                  'assets/icons/facebook.png',
                                  height: 40,
                                ),
                              ),
                              wspacing,
                              ClipOval(
                                child: Image.asset(
                                  'assets/icons/google.png',
                                  height: 40,
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
