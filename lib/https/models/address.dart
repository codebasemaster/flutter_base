// import 'dart:ffi';

import 'package:bi_smartie/https/models/Model.dart';

class Address extends Model {
  int? _provinceId;
  int? _districtId;
  int? _communeId;
  int? _villageId;
  get provinceId => _provinceId;

  set provinceId(value) => _provinceId = value;

  get districtId => _districtId;

  set districtId(value) => _districtId = value;

  get communeId => _communeId;

  set communeId(value) => _communeId = value;

  get villageId => _villageId;

  set villageId(value) => _villageId = value;
  Address(this._communeId, this._districtId, this._provinceId, this._villageId);
  Map<String, dynamic> toJson() {
    return {
      "provinceId": provinceId,
      "districtId": districtId,
      "villageId": villageId,
      "communeId": communeId,
    };
  }

  Address.fromJson(Map<String, dynamic> json) {
    provinceId = json["provinceId"];
    districtId = json["distictId"];
    villageId = json["villageId"];
    communeId = json["communeId"];
  }
}
