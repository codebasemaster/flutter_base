import 'package:flutter/material.dart';

final themes = <ThemeData>[
  ThemeData.light(),
  ThemeData.dark(),
];

final brightness = [Brightness.light, Brightness.dark];

final thememodes = [ThemeMode.light, ThemeMode.dark, ThemeMode.system];
const Color COLOR_SWATCH_D = Colors.black54;
const Color COLOR_BACKGROUND_D = Colors.transparent;
// const Color COLOR_BACKGROUND_D = Colors.black87;
const Color COLOR_BUTTON_D = Colors.blueAccent;
const Color COLOR_PRIMARY_D = Colors.white;
const Color COLOR_ACCENT_D = Colors.black87;
const Color COLOR_SECOND_D = Colors.black54;
const Color COLOR_LESS_D = Colors.black12;
const Color colorLabel = Color(0x00c8c8c8);
const Color colorShadowField = Color.fromRGBO(0, 0, 0, 0.25);
const Color COLOR_SWATCH_W = Colors.white;
const Color COLOR_BACKGROUND_W = Color.fromARGB(255, 222, 180, 14);
// const Color COLOR_BACKGROUND_W = Colors.white10;
const Color COLOR_BUTTON_W = Colors.blueAccent;
const Color COLOR_PRIMARY_W = Colors.black87;
const Color COLOR_ACCENT_W = Colors.white70;
const Color COLOR_SECOND_W = Colors.white60;
const Color COLOR_LESS_W = Colors.white12;
const Color colorHintText = Color.fromARGB(255, 116, 109, 109);
const Color colorApp = COLOR_BACKGROUND_W;
const Color colorAppTranparent = Color.fromARGB(76, 6, 153, 62);
const Color colorOpacity = Color.fromARGB(127, 36, 136, 59);

const Color floatingLabelStyle = Color.fromARGB(255, 206, 206, 206);
const Color hintTextStyle = floatingLabelStyle;
const Color hintLabelStyle = floatingLabelStyle;
const Color labelStyle = Color.fromARGB(255, 143, 143, 143);
