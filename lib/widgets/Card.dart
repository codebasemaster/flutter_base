import 'package:flutter/material.dart';

Widget boxCard({
  double? width,
  double? height,
  EdgeInsets? padding,
  EdgeInsets? margin,
  Widget? header,
  Color? shadowColor,
  Color? color,
  double? elevation,
  bool? clip = true,
  double? borderRadiusCard,
  void Function()? onTap,
  required Widget child,
}) {
  return Builder(builder: (context) {
    final Size size = MediaQuery.of(context).size;
    late double radius = 18.0;
    return InkWell(
      // hoverColor: Colors.transparent,
      focusColor: Colors.transparent,
      borderRadius: BorderRadius.circular(radius),
      onTap: onTap,
      child: Card(
        shadowColor: shadowColor,
        elevation: elevation ?? 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular((borderRadiusCard ?? radius)),
        ),
        color: color != Colors.transparent
            ? color
            : Theme.of(context).colorScheme.surfaceVariant,
        child: Center(
          child: Container(
            height: height,
            width: width,
            padding: color == Colors.transparent
                ? const EdgeInsets.all(0.0)
                : padding ?? const EdgeInsets.all(14.0),
            margin: color == Colors.transparent
                ? const EdgeInsets.all(0.0)
                : margin ?? const EdgeInsets.all(0),
            child: Column(
              children: [
                header ?? const SizedBox(),
                ClipRRect(
                    borderRadius: clip == true
                        ? BorderRadius.circular((borderRadiusCard ?? radius) -
                            ((borderRadiusCard ?? radius) * 15 / 100))
                        : BorderRadius.circular(0),
                    child: child),
              ],
            ),
          ),
        ),
      ),
    );
  });
}

class FilledCard extends StatelessWidget {
  const FilledCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        elevation: 0,
        color: Theme.of(context).colorScheme.surfaceVariant,
        child: const SizedBox(
          width: 300,
          height: 100,
          child: Center(child: Text('Filled Card')),
        ),
      ),
    );
  }
}

class ElevatedCard extends StatelessWidget {
  const ElevatedCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Card(
        child: SizedBox(
          width: 300,
          height: 100,
          child: Center(child: Text('Elevated Card')),
        ),
      ),
    );
  }
}

class OutlinedCard extends StatelessWidget {
  const OutlinedCard({
    Key? key,
    this.child,
  }) : super(key: key);
  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        elevation: 0,
        shape: RoundedRectangleBorder(
          side: BorderSide(
            color: Theme.of(context).colorScheme.outline,
          ),
          borderRadius: const BorderRadius.all(Radius.circular(12)),
        ),
        child: const SizedBox(width: 300, height: 100),
      ),
    );
  }
}
