import 'dart:io';

class FileLogger extends Logger {
  File file;
  FileLogger(this.file);
  @override
  void log(String msg) {
    String logMsg = '${DateTime.now()} $msg \n';
    file.writeAsString(logMsg, mode: FileMode.append);
  }
}

class ConsoleLogger extends Logger {
  @override
  void log(String msg) {
    print('${DateTime.now()} : $msg');
  }
}

abstract class Logger {
  void log(String msg);
}
