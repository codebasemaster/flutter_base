import 'package:flutter/material.dart';

class CustomRouterPage extends PageRouteBuilder {
  final Widget child;
  final AxisDirection? direction;

  CustomRouterPage({
    Key? key,
    this.direction,
    required this.child,
  }) : super(
          pageBuilder: (context, animation, secondaryAnimation) => child,
        );
  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondAnimation, Widget child) {
    const curve = Curves.ease;
    return SlideTransition(
      position: animation.drive(
        Tween<Offset>(
          begin: getOffset(),
          end: Offset.zero,
        ).chain(
          CurveTween(curve: curve),
        ),
      ),
      child: child,
    );
  }

  Offset getOffset() {
    switch (direction) {
      case AxisDirection.up:
        return const Offset(0, 1);
      case AxisDirection.down:
        return const Offset(0, -1);
      case AxisDirection.left:
        return const Offset(-1, 0);
      case AxisDirection.right:
        return const Offset(1, 0);
      default:
        return const Offset(1, 0);
    }
  }
}
