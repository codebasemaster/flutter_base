import 'package:bi_smartie/assets/image.dart';
import 'package:bi_smartie/screens/index.dart';
import 'package:bi_smartie/utilities/index.dart';
import 'package:bi_smartie/widgets/auth/base_auth_field_widget.dart';
import 'package:bi_smartie/widgets/inputField.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class PageUser extends StatefulWidget {
  const PageUser({Key? key}) : super(key: key);

  @override
  _PageUserState createState() => _PageUserState();
}

class _PageUserState extends State<PageUser> {
  bool? isList;
  bool isShowList = true;
  int? showItemList;

  @override
  void initState() {
    showItemList = 6;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final sizeScreen = MediaQuery.of(context).size;
    return PageEdit(
      title: 'Profile',
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.only(bottom: 8.0, left: 8.0),
              child: Text(
                'Account Information',
                textAlign: TextAlign.left,
              ),
            ),
            boxCard(
              color: Colors.transparent,
              child: Column(
                children: [
                  childCard(
                    icon: const Icon(Icons.person_rounded),
                    child: const Text('Profile'),
                  ),
                  childCard(
                    icon: const Icon(Icons.key_off_outlined),
                    child: const Text('Change Password'),
                  )
                ],
              ),
            ),
            hspacing,
            const Padding(
              padding: EdgeInsets.only(bottom: 8.0, left: 8.0),
              child: Text(
                'My Subscription',
                textAlign: TextAlign.left,
              ),
            ),
            boxCard(
              color: Colors.transparent,
              child: Column(
                children: [
                  childCard(
                    child: const Text('Requested Services'),
                  ),
                  childCard(
                    child: const Text('Subscribed Forum'),
                  )
                ],
              ),
            ),
            hspacing,
            const Padding(
              padding: EdgeInsets.only(bottom: 8.0, left: 8.0),
              child: Text(
                'Setting and Privacy',
                textAlign: TextAlign.left,
              ),
            ),
            boxCard(
              color: Colors.transparent,
              child: Column(
                children: [
                  childCard(
                    icon: const Icon(Icons.language_rounded),
                    child: const Text('Language'),
                  ),
                  childCard(
                    icon: const Icon(Icons.settings_input_component),
                    child: const Text('Terms of Services'),
                  ),
                  childCard(
                    icon: const Icon(Icons.privacy_tip),
                    child: const Text('Privacy Policies'),
                  )
                ],
              ),
            ),
            hspacing,
            const Padding(
              padding: EdgeInsets.only(bottom: 8.0, left: 8.0),
              child: Text(
                'Support & Request',
                textAlign: TextAlign.left,
              ),
            ),
            boxCard(
              color: Colors.transparent,
              child: Column(
                children: [
                  childCard(
                    icon: const Icon(Icons.info_rounded),
                    child: const Text('About Us'),
                  ),
                  childCard(
                    icon: const Icon(Icons.contact_mail),
                    child: const Text('Contact Us'),
                  ),
                  childCard(
                    icon: const Icon(Icons.art_track_sharp),
                    child: const Text('Send Request'),
                  ),
                  childCard(
                    icon: const Icon(Icons.question_answer),
                    child: const Text('Send Your Feedback'),
                  )
                ],
              ),
            ),
            hspacing,
            elevateButtonAuth(() {}, sizeScreen, 'Sign Out'),
          ],
        ),
      ),
    );
  }
}

Widget boxUnion(Image image,
    {void Function()? onTap, String? title, bool? isStack = false}) {
  return Builder(builder: (BuildContext context) {
    final double width = MediaQuery.of(context).size.width / 3 - 19.5;
    final double height = (width / 1.5).toDouble();
    return Container(
      height: height + (title != null ? 35 : 0),
      color: isStack == true ? Colors.white : Colors.transparent,
      margin: isStack == true ? const EdgeInsets.all(8.0) : null,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          boxCard(
            onTap: onTap,
            padding: const EdgeInsets.all(0),
            margin: const EdgeInsets.all(0),
            // color: const Color.fromARGB(255, 57, 174, 241),
            // color: isStack == true ? Colors.transparent : Colors.white,
            borderRadiusCard: 7.0,
            shadowColor: Colors.grey.shade300,
            elevation: isStack == true ? 0 : 3,
            clip: true,
            child: SizedBox(
                height: height - (isStack == true ? 20 : 0),
                width: MediaQuery.of(context).size.width / 3 -
                    19.5 -
                    (isStack == true ? 20 * 2 : 0),
                child: image),
          ),
          hspacing,
          title != null
              ? Expanded(
                  child: Text(
                    title.toString(),
                    maxLines: 2,
                    textAlign: TextAlign.center,
                  ),
                )
              : const SizedBox(),
        ],
      ),
    );
  });
}

Widget boxStack(Image image, {void Function()? onTap, String? title}) {
  return Builder(builder: (BuildContext context) {
    final double width = MediaQuery.of(context).size.width / 3 - 19.5;
    // final double height = (width / 1.5).toDouble();
    return SizedBox(
      // height: MediaQuery.of(context).size.height,
      child: Stack(
        alignment: AlignmentDirectional.center,
        children: [
          boxCard(
            onTap: onTap,
            padding: const EdgeInsets.all(0),
            margin: const EdgeInsets.all(0),
            // color: const Color.fromARGB(255, 57, 174, 241),
            borderRadiusCard: 7.0,
            shadowColor: Colors.grey.shade300,
            elevation: 3,
            child: SizedBox(
              height: width / 2,
              width: width,
              child: image,
            ),
          ),
          title != null
              ? Expanded(
                  child: Text(
                    title.toString(),
                    maxLines: 2,
                    textAlign: TextAlign.center,
                  ),
                )
              : const SizedBox(),
        ],
      ),
    );
  });
}
