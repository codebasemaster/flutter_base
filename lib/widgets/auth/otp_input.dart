import 'package:bi_smartie/utilities/auth/base_usage.dart';
import 'package:bi_smartie/utilities/sidebar_base.dart';
import 'package:flutter/material.dart';

class OtpInput extends StatelessWidget {
  final TextEditingController controller;
  final bool autoFocus;
  final String hintText;

  const OtpInput(this.controller, this.autoFocus, this.hintText, {Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 45,
      width: 45,
      child: Container(
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
          boxShadow: const [boxShadow],
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
        ),
        child: TextFormField(
          controller: controller,
          autofocus: autoFocus,
          maxLength: 1,
          cursorColor: Theme.of(context).backgroundColor,
          textAlign: TextAlign.center,
          keyboardType: TextInputType.number,
          decoration: decorationOtp(hintText),
          onChanged: (value) {
            if (value.length == 1) {
              FocusScope.of(context).nextFocus();
            }
          },
        ),
      ),
    );
  }
}
