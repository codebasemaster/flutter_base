import 'dart:convert';
import 'package:bi_smartie/https/models/User.dart';
import 'package:bi_smartie/https/services/index.dart';
import 'package:http/http.dart' as http;

class AuthService extends Service {
  AuthService() : super(url: ENV.apiURL);
  String locationUrl = ENV.locationUrl;

  Future logIn(AuthUser user) async {
    final response = await post(uri: '/login', body: user.submitLogin());
    return response;
  }

  Future register(AuthUser user) async {
    var response = await post(uri: '/register', body: user.submitRegister());
    return response;
  }

  List<AuthUser> parseAuth(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<AuthUser>((json) => AuthUser.formJson(json)).toList();
  }

  Future generateToken(AuthUser user) async {
    return await post(uri: "/generate_token_phone", body: user.submitOTP(), header: {});
  }

  Future getLocationDevice() async {
    try {
      final response = await http.post(Uri.parse(locationUrl));
      int status = response.statusCode;
      if (status == 200 || status == 201) {
        try {
          return jsonDecode(response.body);
        } catch (e) {
          return response.body;
        }
        // return jsonDecode(response.body);
      } else {
        throw (response.body);
      }
    } catch (e) {
      return null;
    }
    // return await post(uri: "", body: "", header: {});
  }

  Future logout(String token) async {
    return await post(uri: "/logout", body: {"token": token});
  }

  Future resetPassword(String uid, String password) async {
    return await post(uri: "/reset_password", body: {"uid": uid, "password": password});
  }

  Future verifyToken(String? token, header) async {
    return await post(uri: "/verifycode", body: "", header: header);
  }

  Future getNewToken(String? refreshToken) async {
    return await post(uri: "/token", body: {"token": refreshToken});
  }
}
