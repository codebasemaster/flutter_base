import '../models/User.dart';

abstract class IAuthFacade {
  Future<void> signIn({
    required String email,
    required String password,
  });
  Future<void> register(
      {required String username,
      required String email,
      required String password});
  Future<AuthUser?> getUser();
  Future<void> logOut();
}
