import 'package:flutter/material.dart';

Widget tickWidget(String msg, int status) {
  //1 is for tick 2 for red 3 for warning
  Color text = status == 2 ? Colors.black : Colors.white;
  Color colorWidget =
      status == 1 ? const Color.fromARGB(129, 76, 175, 79) : Colors.redAccent;
  return Container(
    padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(10.0),
      color: colorWidget,
    ),
    child: Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Icon(
          Icons.check,
          color: text,
        ),
        const SizedBox(
          width: 12.0,
        ),
        Text(
          msg,
          style: const TextStyle(
            color: Colors.black,
          ),
        ),
      ],
    ),
  );
}
