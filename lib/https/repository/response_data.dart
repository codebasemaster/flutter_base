Map<String, dynamic> responseJson(dynamic data, int status) {
  return {
    "data": data,
    "status": status,
  };
}
