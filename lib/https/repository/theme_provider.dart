import 'package:flutter/material.dart';
import 'package:bi_smartie/App.dart';

const Color bg = Color(0xFF24883B);

// const Color COLOR_BACKGROUND_W = Color.fromARGB(255, 6, 153, 62);
// const Color COLOR_BACKGROUND_D = Colors.transparent;
const String fontFamily = 'MuraHand';

final ThemeData lightTheme = ThemeData(
  fontFamily: fontFamily,
  brightness: Brightness.light,
  primaryColor: COLOR_PRIMARY_W,
  backgroundColor: bg,
  colorScheme: ColorScheme.fromSwatch().copyWith(secondary: Colors.blue),
);
final ThemeData darkTheme = ThemeData(
  fontFamily: fontFamily,
  brightness: Brightness.dark,
  primaryColor: COLOR_PRIMARY_D,
  backgroundColor: COLOR_BACKGROUND_D,
  // colorScheme: ColorScheme.fromSwatch().copyWith(secondary: Colors.red),
);

class ThemeProvider extends ChangeNotifier {
  ThemeData _themeData = lightTheme;
  ThemeData getTheme() => _themeData;

  String _currentMode = 'auto';

  ThemeMode _themeMode = ThemeMode.light;

  get currentMode => _currentMode;
  get themeMode => _themeMode;

  int _index = 0; // index 0 is light
  int get index => _index;

  // contractore
  ThemeProvider() {
    SharePreferences.readData('systemMode').then((value) {
      _currentMode = value ?? 'light';
    });
    SharePreferences.readData('themeMode').then((value) {
      var themeMode = (value == 'auto') ? _currentMode : value;
      _currentMode = value ?? 'dark';
      if (themeMode == 'dark') {
        _themeData = darkTheme;
      } else {
        _themeData = lightTheme;
      }
      notifyListeners();
    });
  }
  static setSysTheme(String value) {
    SharePreferences.saveData('systemMode', value);
  }

  void setSysMode() {
    const String value = 'auto';
    _currentMode = value;
    SharePreferences.saveData('themeMode', value);
    notifyListeners();
  }

  void setDarkMode() async {
    const String value = 'dark';
    _themeData = darkTheme;
    _currentMode = value;
    SharePreferences.saveData('themeMode', value);
    notifyListeners();
  }

  void setLightMode() async {
    const String value = 'light';
    _themeData = lightTheme;
    _currentMode = value;
    SharePreferences.saveData('themeMode', value);
    notifyListeners();
  }

  void toggleTheme(bool isDark) {
    _themeMode = isDark ? ThemeMode.dark : ThemeMode.light;
    _index = isDark ? 1 : 0;
    isDark ? setDarkMode() : setLightMode();
    notifyListeners();
  }

  void setTheme() {
    _index = _index == 1 ? 0 : 1;
    _themeMode = _index == 1 ? ThemeMode.dark : ThemeMode.light;
    _index == 1 ? setDarkMode() : setLightMode();
    notifyListeners();
  }

  void setDefault() {
    _index = _index == 1 ? 0 : 1;
    notifyListeners();
  }

  void setDark() {
    _index = 1;
    _themeMode = ThemeMode.dark;
    setDarkMode();
    notifyListeners();
  }

  void setLight() {
    _index = 0;
    _themeMode = ThemeMode.light;
    setLightMode();
    notifyListeners();
  }
}
