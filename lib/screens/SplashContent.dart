import 'dart:async';
import 'dart:convert';
import 'package:bi_smartie/App.dart';
import 'package:flutter/material.dart';

class SplashContent extends StatefulWidget {
  const SplashContent({Key? key}) : super(key: key);

  @override
  SplashContentState createState() => SplashContentState();
}

class SplashContentState extends State<SplashContent>
    with SingleTickerProviderStateMixin {
  late String screenNavigate = ['login', 'dev', 'sm'][2];
  AuthUser? user;

  startTimeout() {
    var duration = const Duration(seconds: 1);
    return Timer(duration, handleTimeout);
  }

  void handleTimeout() {
    Navigator.pushReplacementNamed(context, screenNavigate);
  }

  void fetchData() async {
    try {
      startTimeout();
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    super.initState();
    startTimeout();
  }

  @override
  Widget build(BuildContext context) {
    final String currentMode = MediaQuery.of(context)
        .platformBrightness
        .toString()
        .replaceAll('Brightness.', '');
    ThemeProvider.setSysTheme(currentMode);

    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              logoPath,
              fit: BoxFit.cover,
              height: 170,
              width: 170,
            ),
            const SizedBox(height: 45),
            const SizedBox(
              width: 300,
              child: Text(
                'Empower Yourself Through Education',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  color: Color.fromARGB(255, 154, 125, 10),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
