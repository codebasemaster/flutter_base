import 'package:bi_smartie/App.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:indexed/indexed.dart';

// ignore: must_be_immutable
class NavigationBottom extends StatelessWidget {
  void Function(int)? onTap;
  Color? selectedColor;
  Color? uselessColor;
  int indexScreen = 0;
  List screens = [];
  Color? color;
  final double iconH = 25.0;
  final double iconW = 25.0;

  NavigationBottom({
    Key? key,
    required this.screens,
    required this.indexScreen,
    this.color,
    this.selectedColor,
    this.uselessColor,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    late Color primeColor = uselessColor ?? Colors.grey;
    late Color defualtColor = selectedColor ?? colorApp;
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      backgroundColor: color ??
          Theme.of(context).scaffoldBackgroundColor, //and one more is fixed
      elevation: 0,
      showUnselectedLabels: true,
      selectedFontSize: 12.0,
      unselectedFontSize: 8.0,
      unselectedItemColor: primeColor,
      selectedItemColor: defualtColor,
      currentIndex: indexScreen,
      onTap: onTap, //New
      items: [
        BottomNavigationBarItem(
          icon: SvgPicture.asset(
            'assets/icons/home.svg',
            color: indexScreen == 0 ? selectedColor : uselessColor,
            width: iconW,
            height: iconH,
            fit: BoxFit.fill,
          ),
          label: 'home'.tr(),
        ),
        BottomNavigationBarItem(
          icon: SvgPicture.asset(
            'assets/icons/discussion.svg',
            color: indexScreen == 1 ? selectedColor : uselessColor,
            width: iconW,
            height: iconH,
            fit: BoxFit.cover,
          ),
          label: 'forum'.tr(),
        ),
        BottomNavigationBarItem(
          icon: Indexer(
            children: [
              Indexed(
                index: 0,
                child: SvgPicture.asset(
                  'assets/icons/news.svg',
                  color: indexScreen == 2 ? selectedColor : uselessColor,
                  width: iconW,
                  height: iconH,
                  fit: BoxFit.fill,
                ),
              )
            ],
          ),
          label: 'news'.tr(),
        ),
        BottomNavigationBarItem(
          icon: SvgPicture.asset(
            'assets/icons/products.svg',
            color: indexScreen == 3 ? selectedColor : uselessColor,
            width: iconW,
            height: iconH,
            fit: BoxFit.fill,
          ),
          label: 'services'.tr(),
        ),
        BottomNavigationBarItem(
          icon: SvgPicture.asset(
            'assets/icons/account.svg',
            color: indexScreen == 4 ? selectedColor : uselessColor,
            width: iconW,
            height: iconH,
            fit: BoxFit.fill,
          ),
          label: 'account'.tr(),
        ),
      ],
    );
  }
}
