import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Future datePicker(BuildContext ctx,
    {required void Function(dynamic date) onDateTimeChanged,
    String? initialDateTime,
    required void Function()? onPressed,
    required compareDate}) {
  dynamic timestamps;
  void dateChange(date) {
    timestamps = date;
  }

  return showCupertinoModalPopup(
    context: ctx,
    builder: (_) => Container(
      height: 300,
      margin: const EdgeInsets.symmetric(horizontal: 14),
      padding: const EdgeInsets.only(bottom: 14),
      child: Column(
        children: [
          Expanded(
            child: ClipRRect(
              borderRadius: const BorderRadius.all(Radius.circular(16)),
              child: Container(
                color: Colors.white,
                child: CupertinoTheme(
                  data: const CupertinoThemeData(
                    textTheme: CupertinoTextThemeData(
                      dateTimePickerTextStyle: TextStyle(
                          fontSize: 16,
                          fontFamily: 'MuraHand',
                          color: Colors.black),
                    ),
                  ),
                  child: CupertinoDatePicker(
                    minimumDate: compareDate,
                    mode: CupertinoDatePickerMode.date,
                    initialDateTime: (initialDateTime != null
                        ? DateTime.parse(initialDateTime)
                        : DateTime.now()),
                    onDateTimeChanged: (date) {
                      dateChange(date);
                      onDateTimeChanged(timestamps);
                    },
                  ),
                ),
              ),
            ),
          ),
          // Close the modal
          Container(
            height: 70,
            width: MediaQuery.of(ctx).size.width,
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: ClipRRect(
                borderRadius: const BorderRadius.all(Radius.circular(16)),
                child: Container(
                  height: 46,
                  color: Colors.white,
                  child: TextButton(
                    onPressed: onPressed,
                    child: Text('word.ok'.tr().toUpperCase()),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    ),
  );
}
