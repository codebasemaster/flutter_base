// FutureBuilder<String>(
//   future: stringAssets.load('hello.txt'),
//   builder: (context, snapshot) {
//     if (snapshot.hasData) {
//       return Text(snapshot.data ?? '');
//     } else {
//       return const Text('loading..');
//     }
//   },
// ),
// FutureBuilder<String>(
//   future: stringAssetsWithoutBasePath
//       .load('assets/strings/hello-new-string.txt'),
//   builder: (context, snapshot) {
//     if (snapshot.hasData) {
//       return Text(snapshot.data ?? '');
//     } else {
//       return const Text('loading..');
//     }
//   },
// ),
// FutureBuilder<dynamic>(
//   future: jsonAssets.load('sprite.json'),
//   builder: (context, snapshot) {
//     if (snapshot.hasData) {
//       return Text(snapshot.data.toString());
//     } else {
//       return const Text('loading..');
//     }
//   },
// ),
// FutureBuilder<ui.Image>(
//   future: imageAssets.load('angel.png'),
//   builder: (context, snapshot) {
//     if (snapshot.hasData) {
//       return CustomPaint(
//         painter: MyImagePainter(snapshot.data!),
//       );
//     } else {
//       return const Text('loading..');
//     }
//   },
// ),
// FutureBuilder<ByteData>(
//   future: byteAssets.load('angel.png'),
//   builder: (context, snapshot) {
//     if (snapshot.hasData) {
//       return Image.memory(snapshot.data!.buffer.asUint8List());
//     } else {
//       return const Text('loading..');
//     }
//   },
// ),
