import 'package:bi_smartie/App.dart';
import 'package:flutter/material.dart';

Widget DropdownMenu({
  Text? hint,
  double? width,
  double? height,
  bool? margin,
  bool? singlefeild,
  Color? bgColor,
  Color? color,
  required dynamic selecteValue,
  required dynamic itemValue,
  required dynamic displayName,
  required List items,
  required void Function(dynamic value) onChanged,
}) {
  return SizedBox(
    width: width,
    height: height,
    child: Container(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      margin: (singlefeild == false) ? const EdgeInsets.all(5.0) : null,
      decoration: BoxDecoration(
        boxShadow: const [boxShadow],
        borderRadius: BorderRadius.circular(8.0),
        color: bgColor ?? Colors.white,
      ),
      child: DropdownButton<dynamic>(
        dropdownColor: Colors.white,
        elevation: 16,
        value: selecteValue,
        borderRadius: borderRadius,
        autofocus: true,
        isExpanded: true,
        hint: hint,
        underline: Container(color: Colors.transparent),
        items: items.map((item) {
          return DropdownMenuItem<dynamic>(
            value: item[itemValue],
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              margin: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Text(item[displayName].toString(), style: const TextStyle(fontSize: 13.5)),
            ),
          );
        }).toList(),
        onChanged: onChanged,
      ),
    ),
  );
}
