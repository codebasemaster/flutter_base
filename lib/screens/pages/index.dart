import 'package:bi_smartie/screens/index.dart';
import 'package:flutter/material.dart';

export 'page_register.dart';
export 'page_login.dart';

export './page_login.dart' show PageLogin;

enum PageType { school, scholarship, health, home, forum, news, account, profile, login }

// ignore: must_be_immutable
class CustomPage extends StatelessWidget {
  PageType? pageType = PageType.home;
  CustomPage({Key? key, this.pageType}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    switch (pageType) {
      case PageType.school:
        return PageEdit(
          title: 'School/Universities',
        );
        break;
      case PageType.account:
        return PageEdit(
          title: 'Profile',
        );
        break;
      case PageType.login:
        return PageEdit(
          title: 'School',
        );
        break;
      default:
    }
    return Scaffold();
  }
}
