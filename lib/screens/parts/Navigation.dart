import 'dart:io';

import 'package:bi_smartie/App.dart';
import 'package:flutter/material.dart';

Widget Navigation({bool? isShow = true}) {
  late double appSize = (Platform.isIOS || Platform.isAndroid) ? 44 : 10;
  return PreferredSize(
    preferredSize: Size(0, isShow == true ? 65.0 : 0),
    child: Container(
      padding:
          EdgeInsets.only(left: 16.0, right: 4.0, top: appSize, bottom: 8.0),
      color: Colors.transparent,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          wspacing,
          Padding(
            padding: const EdgeInsets.only(left: 25),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                  logoPath,
                  fit: BoxFit.cover,
                  height: 25,
                  width: 25,
                ),
                wspacing,
                Text(
                  'BiStartie',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      color: fontColor),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 14.0),
            child: Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(radius)),
                  child: Image.asset(langEn, width: 24, fit: BoxFit.cover),
                ),
                const SizedBox(width: 5),
                ClipRRect(
                  borderRadius: const BorderRadius.all(Radius.circular(2)),
                  child: Image.asset(langKh, width: 25, fit: BoxFit.cover),
                ),
              ],
            ),
          )
        ],
      ),
    ),
  );
}
