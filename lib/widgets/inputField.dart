import 'package:bi_smartie/widgets/auth/base_auth_field_widget.dart';
import 'package:flutter/material.dart';

class InputField extends BaseAuthFieldWidget {
  InputField({
    super.key,
    super.screen,
    super.label,
    super.controller,
    super.prefixicon,
    super.subfixicon,
    super.validator,
    super.enabled,
    super.iconSubfix,
    super.onChanged,
    super.hintText,
    super.keyBoardType,
    super.onFocus,
    super.maxLines,
    super.isDense,
    super.notShadow,
    super.inputFormattersCustom,
    required super.secureText,
    super.setStatusIcon,
  });
}
