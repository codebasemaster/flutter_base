import 'package:bi_smartie/https/models/Model.dart';

class Province implements BaseModel {
  int? id;
  String? nameKh;
  String? nameEn;
  String? geoCode;

  Province({this.id, this.nameKh, this.nameEn, this.geoCode});

  @override
  Map<String, dynamic> fetchData() {
    print("fetch data");
    return Map();
  }

  Province.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameKh = json['name_kh'];
    nameEn = json['name_en'];
    geoCode = json['geo_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name_kh'] = nameKh;
    data['name_en'] = nameEn;
    data['geo_code'] = geoCode;
    return data;
  }
}

class District extends Model {
  int? id;
  int? provinceId;
  String? nameKh;
  String? nameEn;
  String? geoCode;

  District({this.id, this.provinceId, this.nameKh, this.nameEn, this.geoCode});

  District.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    provinceId = json['province_id'];
    nameKh = json['name_kh'];
    nameEn = json['name_en'];
    geoCode = json['geo_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['province_id'] = provinceId;
    data['name_kh'] = nameKh;
    data['name_en'] = nameEn;
    data['geo_code'] = geoCode;
    return data;
  }
}

class Commune extends Model {
  int? id;
  int? districtId;
  String? nameKh;
  String? nameEn;
  String? geoCode;

  Commune({this.id, this.districtId, this.nameKh, this.nameEn, this.geoCode});

  Commune.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    districtId = json['district_id'];
    nameKh = json['name_kh'];
    nameEn = json['name_en'];
    geoCode = json['geo_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['district_id'] = districtId;
    data['name_kh'] = nameKh;
    data['name_en'] = nameEn;
    data['geo_code'] = geoCode;
    return data;
  }
}

class Village extends Model {
  int? id;
  int? communeId;
  String? nameKh;
  String? nameEn;
  String? geoCode;

  Village({this.id, this.communeId, this.nameKh, this.nameEn, this.geoCode});

  Village.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    communeId = json['commune_id'];
    nameKh = json['name_kh'];
    nameEn = json['name_en'];
    geoCode = json['geo_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['commune_id'] = communeId;
    data['name_kh'] = nameKh;
    data['name_en'] = nameEn;
    data['geo_code'] = geoCode;
    return data;
  }
}
