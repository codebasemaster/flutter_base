import 'package:bi_smartie/assets/image.dart';
import 'package:bi_smartie/screens/index.dart';
import 'package:bi_smartie/utilities/index.dart';
import 'package:flutter/material.dart';

class PageUniversity extends StatefulWidget {
  const PageUniversity({Key? key}) : super(key: key);

  @override
  _PageUniversityState createState() => _PageUniversityState();
}

class _PageUniversityState extends State<PageUniversity> {
  bool? isList;
  bool isShowList = true;
  int? showItemList;
  List<Image> promotions = <Image>[
    Image.asset('assets/images/promotion.png'),
    Image.asset('assets/images/promotion.png'),
    Image.asset('assets/images/promotion.png'),
    Image.asset('assets/images/promotion.png'),
    Image.asset('assets/images/promotion.png'),
    Image.asset('assets/images/promotion.png'),
  ];
  List<Widget> itemListSchool = <Widget>[
    favSchool(
      Image.asset('assets/icons/figure-university.png', height: 65.5),
      const FittedBox(child: Text('Public Schools')),
    ),
    favSchool(
      Image.asset('assets/icons/figure-private-school.png', height: 65.5),
      const FittedBox(child: Text('Private Schools')),
    ),
    favSchool(
      Image.asset('assets/icons/training-center.png', height: 65.5),
      const FittedBox(child: Text('Training Schools')),
    ),
  ];
  List<Widget> itemPartnerSchool = <Widget>[
    boxUnion(
      Image.asset(
        'assets/icons/itc-logo.png',
        fit: BoxFit.contain,
      ),
      title: 'ITC',
      onTap: () {},
    ),
    boxUnion(
      Image.asset(
        'assets/icons/itc-logo.png',
        fit: BoxFit.contain,
      ),
      title: 'IFL',
      onTap: () {},
    ),
    boxUnion(
      Image.asset(
        'assets/icons/itc-logo.png',
        fit: BoxFit.contain,
      ),
      title: 'IFL',
      onTap: () {},
    ),
    boxUnion(
      Image.asset(
        'assets/icons/itc-logo.png',
        fit: BoxFit.contain,
      ),
      title: 'IFL',
      onTap: () {},
    ),
    boxUnion(
      Image.asset(
        'assets/icons/itc-logo.png',
        fit: BoxFit.contain,
      ),
      title: 'IFL',
      onTap: () {},
    ),
    boxUnion(
      Image.asset(
        'assets/icons/itc-logo.png',
        fit: BoxFit.contain,
      ),
      title: 'IFL',
      onTap: () {},
    ),
  ];
  List<Widget> itemList = <Widget>[
    boxUnion(imgNewsComp,
        title: 'Schools, Universities & Training Centers', onTap: () {}),
    boxUnion(imgForumComp, title: 'Skill Orientation', onTap: () {}),
    boxUnion(imgScholarshipComp, title: 'Discussion', onTap: () {}),
    boxUnion(imgScholarshipComp, title: 'Student Loan', onTap: () {}),
    boxUnion(imgNewsComp, title: 'Events', onTap: () {}),
    boxUnion(imgForumComp, title: 'Public Figures', onTap: () {})
  ];
  List<Widget> allItemList = [];

  @override
  void initState() {
    showItemList = 6;
    allItemList = itemList.sublist(0, showItemList);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return PageEdit(
      title: 'Schools/Universities',
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(18.0),
              child: promotions[0],
            ),
            hspacing,
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: promotions
                  .map((e) => Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: ClipOval(
                          child: Container(
                            width: 5,
                            height: 5,
                            color: colorApp,
                            child: const SizedBox(),
                          ),
                        ),
                      ))
                  .toList(),
            ),
            hspacing,
            AnimatedContainer(
              duration: const Duration(milliseconds: 3000),
              curve: Curves.bounceIn,
              child: SizedBox(
                // height: !isShowList
                //     ? ((125 * allItemList.length / 3) +
                //         (allItemList.length / 3) * 25)
                //     : 125 * 2,
                height: 125 * 2,
                child: GridView.count(
                  primary: false,
                  padding: const EdgeInsets.all(0),
                  crossAxisSpacing: 5,
                  mainAxisSpacing: 0,
                  crossAxisCount: 3,
                  children: allItemList,
                ),
              ),
            ),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //   children: [
            //     const SizedBox(),
            //     TextButton(
            //       onPressed: () {
            //         setState(() {
            //           allItemList = isShowList
            //               ? itemList
            //               : itemList.sublist(0, showItemList);
            //           isShowList = !isShowList;
            //         });
            //       },
            //       child: Text(
            //         (isShowList ? 'more' : 'less').toCapitalized(),
            //         textAlign: TextAlign.justify,
            //       ),
            //     ),
            //   ],
            // ),
            // hspacing,
            Text('Search for Your Favorite Schools',
                style: TextStyle(color: fontColor, fontSize: 16)),
            hspacing2,
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: InputField(
                    secureText: false,
                    prefixicon: const Icon(Icons.search),
                  ),
                ),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(Icons.sort_rounded),
                ),
              ],
            ),
            hspacing3,
            Container(
              padding: const EdgeInsets.only(bottom: 10),
              height: 140,
              child: GridView.count(
                primary: false,
                padding: const EdgeInsets.all(0),
                crossAxisSpacing: 0,
                mainAxisSpacing: 0,
                crossAxisCount: 3,
                children: itemListSchool,
              ),
            ),
            hspacing,
            const Center(
              child: Text(
                'Our partner universities',
                style: TextStyle(fontSize: 16),
              ),
            ),
            hspacing2,
            SizedBox(
              height: 125 * (itemPartnerSchool.length / 4) + 10,
              width: 350,
              child: GridView.count(
                primary: false,
                padding: const EdgeInsets.all(0),
                crossAxisSpacing: 0,
                mainAxisSpacing: 0,
                crossAxisCount: 4,
                children: itemPartnerSchool,
              ),
            ),
            hspacing,
            Text('Assess yourself',
                style: TextStyle(color: fontColor, fontSize: 16)),
            hspacing2,
            SizedBox(
              height: 125,
              child: GridView.count(
                primary: false,
                padding: const EdgeInsets.all(0),
                crossAxisSpacing: 5,
                mainAxisSpacing: 0,
                crossAxisCount: 3,
                children: [
                  boxUnion(
                    Image.asset(
                      'assets/icons/translation.png',
                      fit: BoxFit.contain,
                    ),
                    title: 'Test Translations ',
                    onTap: () {},
                  ),
                  boxUnion(
                    Image.asset(
                      'assets/icons/group-discussion.png',
                      fit: BoxFit.contain,
                    ),
                    title: 'Working group',
                    onTap: () {},
                  ),
                  boxUnion(
                    Image.asset(
                      'assets/icons/itc-logo.png',
                      fit: BoxFit.contain,
                    ),
                    title: 'Test entrance',
                    onTap: () {},
                  ),
                ],
              ),
            ),
            hspacing,
            Text('Related Services from Our Partners',
                style: TextStyle(color: fontColor, fontSize: 16)),
            hspacing2,
            SizedBox(
              height: 125,
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    boxUnion(
                      Image.asset(
                        'assets/icons/translation.png',
                        fit: BoxFit.cover,
                      ),
                      title: 'Translation',
                      onTap: () {},
                    ),
                    boxUnion(
                      Image.asset(
                        'assets/icons/school-exchange.png',
                        fit: BoxFit.cover,
                      ),
                      title: 'Exchange Program',
                      onTap: () {},
                    ),
                    boxUnion(
                      Image.asset(
                        'assets/icons/visa-flight-ticket.png',
                        fit: BoxFit.cover,
                      ),
                      title: 'Visa & Travel',
                      onTap: () {},
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
