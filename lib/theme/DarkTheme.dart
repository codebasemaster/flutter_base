import 'package:bi_smartie/theme/constants.dart';
import 'package:flutter/material.dart';

ThemeData darkTheme = ThemeData(
  fontFamily: 'Georgia',
  // primarySwatch: CColors.black87,
  primaryColor: COLOR_PRIMARY_D,
  brightness: Brightness.dark,
  backgroundColor: COLOR_BACKGROUND_D,
  dividerColor: COLOR_LESS_D,
  floatingActionButtonTheme: const FloatingActionButtonThemeData(
    backgroundColor: COLOR_BUTTON_D,
    foregroundColor: COLOR_SWATCH_W,
  ),
  switchTheme: SwitchThemeData(
    trackColor: MaterialStateProperty.all<Color>(COLOR_BUTTON_D),
    thumbColor: MaterialStateProperty.all<Color>(COLOR_ACCENT_W),
    overlayColor: MaterialStateProperty.all<Color>(COLOR_ACCENT_D),
    splashRadius: 120.0,
  ),
  inputDecorationTheme: InputDecorationTheme(
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(20.0),
      borderSide: BorderSide.none,
    ),
    filled: true,
    fillColor: COLOR_ACCENT_D.withOpacity(0.1),
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ButtonStyle(
      padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
        const EdgeInsets.symmetric(horizontal: 40.0, vertical: 20.0),
      ),
      shape: MaterialStateProperty.all<OutlinedBorder>(
        RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
      ),
      backgroundColor: MaterialStateProperty.all<Color>(COLOR_BACKGROUND_D),
      foregroundColor: MaterialStateProperty.all<Color>(COLOR_SWATCH_D),
      // overlayColor: MaterialStateProperty.all<Color>(Colors.black26),
    ),
  ),
  colorScheme: ColorScheme.fromSwatch().copyWith(secondary: COLOR_ACCENT_D),
);
