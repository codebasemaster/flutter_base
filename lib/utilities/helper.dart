import 'dart:io';
import 'dart:convert';
import 'package:path/path.dart';
import 'package:async/async.dart';
import 'package:bi_smartie/App.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;

export './string_extension.dart';
export './image_extension.dart';

class Helper {
  static Future<String> fakeDataCollection() async {
    return await rootBundle.loadString('assets/datas/production.json');
  }

  static Future<String> fetchLocal(String namefile) async {
    return await rootBundle.loadString("assets/$namefile");
  }
}

Widget addVerticalSpace(double height) {
  return SizedBox(
    height: height,
  );
}

Widget addHorizontalSpace(double width) {
  return SizedBox(
    width: width,
  );
}

Widget checkDateTime(DateTime value) {
  // dd/mm/yyyy
  final DateTime now = DateTime.now();
  final DateTime lastYear = DateTime.now().add(const Duration(days: -365));
  final DateTime month = DateTime.now().add(const Duration(days: -1));
  final DateTime date = DateTime.now().add(const Duration(days: -1));
  // return now;
  return Container();
}

Widget InputFeild({
  Icon? icon,
  String? label,
  void Function(String)? onChange,
  void Function(String)? onSubmitted,
}) {
  return Container(
    // height: 60,
    decoration: BoxDecoration(
      color: Colors.white,
      borderRadius: borderRadius,
      boxShadow: const [boxShadow],
    ),
    child: TextField(
      decoration: InputDecoration(
        // suffixIconColor: const Color(0xffC4DFCB),
        contentPadding: const EdgeInsets.only(left: 16.0),
        prefixIcon: icon != null ? SizedBox(width: 25, height: 25, child: icon) : null,
        labelStyle: const TextStyle(fontSize: 14.5, color: labelStyle),
        floatingLabelStyle: const TextStyle(fontSize: 14.5, color: floatingLabelStyle),
        focusedBorder: const OutlineInputBorder(borderSide: BorderSide(color: COLOR_BACKGROUND_W)),
        enabledBorder: const OutlineInputBorder(borderSide: BorderSide(color: COLOR_BACKGROUND_D)),
        // border: const OutlineInputBorder(),
        labelText: label,
        // hintText: label,
      ),
      onChanged: onChange,
      onSubmitted: onSubmitted,
    ),
  );
}

Widget imageRound(dynamic image, {double? width, double? height, BorderRadius? borderRadius}) {
  return SizedBox(
    width: width,
    height: height,
    child: ClipRRect(
      borderRadius: borderRadius,
      child: image,
    ),
  );
}

class ToastType {
  String? msg;
  Toast? toastLength;
  ToastGravity? gravity;
  int? timeInSecForIosWeb;
  Color? backgroundColor;
  Color? textColor;
  double? fontSize;
  ToastType({
    required this.msg,
    this.backgroundColor,
    this.fontSize,
    this.gravity,
    this.textColor,
    this.timeInSecForIosWeb,
    this.toastLength,
  });
  void main() async {
    try {
      final check = await Fluttertoast.showToast(
        msg: msg!,
        toastLength: toastLength,
        gravity: gravity,
        timeInSecForIosWeb: timeInSecForIosWeb ?? 1,
        backgroundColor: backgroundColor,
        textColor: textColor,
        fontSize: fontSize,
      );
      if (check == true) {
        print("ToastType: $msg");
      }
    } catch (e) {
      print(e);
    }
  }
}

// ignore: camel_case_types
class cToast {
  final ToastType toast = ToastType(
    toastLength: Toast.LENGTH_SHORT,
    gravity: ToastGravity.CENTER,
    timeInSecForIosWeb: 1,
    backgroundColor: const Color.fromARGB(223, 224, 224, 224),
    textColor: Colors.black38,
    fontSize: 16.0,
    msg: 'Error',
  );

  static Future<void> error({required String message}) async {
    ToastType(
      msg: message,
      backgroundColor: const Color.fromARGB(134, 244, 67, 54),
      textColor: const Color.fromARGB(132, 255, 255, 255),
    );
  }

  static Future<void> succes({required String message}) async {
    ToastType(
      msg: message,
      backgroundColor: const Color.fromARGB(134, 244, 67, 54),
      textColor: const Color.fromARGB(132, 255, 255, 255),
    );
  }

  static Future<void> warn({required String message}) async {
    ToastType(
      msg: message,
      backgroundColor: const Color.fromARGB(242, 6, 153, 62),
      textColor: const Color.fromARGB(132, 255, 255, 255),
    );
  }
}

void showErrorOrSuccess(
    {required String text, required BuildContext context, required bool statusFail}) {
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    backgroundColor: statusFail == true
        ? const Color.fromARGB(157, 238, 160, 154)
        : const Color.fromARGB(185, 178, 250, 180),
    content: Row(
      children: [
        Padding(
          padding: const EdgeInsets.only(right: 5),
          child: Icon(
            statusFail == true ? Icons.warning : Icons.check,
            color: statusFail == true
                ? const Color.fromARGB(255, 153, 35, 35)
                : const Color.fromARGB(255, 42, 119, 45),
            size: 20,
          ),
        ),
        Text(
          text,
          style: TextStyle(
              color: statusFail == true
                  ? const Color.fromARGB(255, 153, 35, 35)
                  : const Color.fromARGB(255, 42, 119, 45),
              fontWeight: FontWeight.bold),
        )
      ],
    ),
  ));
}

void waitingMessage(BuildContext context) {
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      backgroundColor: const Color.fromARGB(131, 0, 0, 0),
      content: Text(
        'auth.register.waiting'.tr(),
      ),
    ),
  );
}
// Function

class Files {
  Upload(File imageFile, String uploadURL) async {
    var stream = http.ByteStream(DelegatingStream(imageFile.openRead()));
    var length = await imageFile.length();

    var uri = Uri.parse(uploadURL);

    var request = http.MultipartRequest("POST", uri);
    var multipartFile =
        http.MultipartFile('file', stream, length, filename: basename(imageFile.path));
    //contentType: new MediaType('image', 'png'));

    request.files.add(multipartFile);
    var response = await request.send();
    print(response.statusCode);
    response.stream.transform(utf8.decoder).listen((value) {
      print(value);
    });
  }

  _asyncFileUpload(String text, File file) async {
    //create multipart request for POST or PATCH method
    var request = http.MultipartRequest("POST", Uri.parse("<url>"));
    //add text fields
    request.fields["text_field"] = text;
    //create multipart using filepath, string or bytes
    var pic = await http.MultipartFile.fromPath("file_field", file.path);
    //add multipart to request
    request.files.add(pic);
    var response = await request.send();

    //Get the response from the server
    var responseData = await response.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);
    print(responseString);
  }
}

dynamic convertToJson(dynamic value) {
  return jsonDecode(jsonEncode(value));
}
//TODO: create two widget one for checking role to access as array and other one is consumer(cont)
