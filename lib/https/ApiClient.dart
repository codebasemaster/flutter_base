import 'package:injectable/injectable.dart';

@injectable // or lazy/singleton
class ApiClient {
  @factoryMethod
  static Future<ApiClient> create(Deps) async {
    return Deps;
  }
}
