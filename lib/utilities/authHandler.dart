import 'dart:convert';

import 'package:bi_smartie/App.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void authHandler(BuildContext context, handler, AuthProvider provider) {
  SharedPreferences.getInstance().then((prefs) {
    final data = prefs.getString("user");
    late String decodeRefreshToken = '';
    if (data != "" || data != null) {
      decodeRefreshToken = jsonDecode(data!)["refreshToken"];
    }
    // final currentPlaform = defaultTargetPlatform;
    // if (currentPlaform != TargetPlatform.linux && currentPlaform != TargetPlatform.windows) {
    //   logoutFunction(context, decodeRefreshToken, prefs, handler, provider);
    // } else {
    // }
    logoutFunction(context, decodeRefreshToken, prefs, handler, provider);
  });
}

void logoutFunction(BuildContext context, String decodeRefreshToken, SharedPreferences prefs,
    String nameRoute, AuthProvider provider) {
  AuthController auth = AuthController();
  waitingMessage(context);
  auth.logOut(decodeRefreshToken).then((value) {
    provider.setUserPro(null);
    prefs.remove("user");
    ScaffoldMessenger.of(context).hideCurrentSnackBar();
    Future.delayed(const Duration(seconds: 2), () {
      Navigator.pushNamedAndRemoveUntil(context, nameRoute, (route) => false);
    });
  }).catchError((onError) {
    provider.setUserPro(null);
    prefs.remove("user");
  });
}
