import 'package:bi_smartie/App.dart';
import 'package:flutter/widgets.dart';

export 'package:bi_smartie/widgets/SidebarWidget.dart' show SidebarCell;
export 'package:bi_smartie/widgets/TabBarViewWidget.dart' show TabBarViewWidget;
export 'package:bi_smartie/widgets/TabBarController.dart' show TabBarController;
export './BaseAppBar.dart' show BaseAppBar;
export './Card.dart' show FilledCard, ElevatedCard, OutlinedCard;
export './BTNSheet.dart' show BTNSheet;
export './showSheet.dart' show ShowSheet;
export './ImageNetwork.dart' show imageNetwork;
export './ChildCard.dart' show childCard;
export './AppAnimate.dart' show appAnimate;
export './MeasureSize.dart' show MeasureSize, WidgetSize;
export './Logo.dart' show AppLogo;
export './inputField.dart' show InputField;
export './widgets.dart';

export 'package:bi_smartie/widgets/Card.dart';
export '../utilities/helper.dart';

export './auth/base_auth_field_widget.dart' show elevateButton;

late List<Widget> appScreens = List.from([
  const ScreenHome(key: PageStorageKey<String>('bi_smartie_ScreenHome')),
  const ScreenForum(key: PageStorageKey<String>('bi_smartie_ScreenForum')),
  const ScreenNews(key: PageStorageKey<String>('bi_smartie_ScreenNews')),
  const ScreenServices(
      key: PageStorageKey<String>('bi_smartie_ScreenServices')),
  const PageLogin(key: PageStorageKey<String>('bi_smartie_PageLogin'))
]);


// Navigator.of(context).push(Transitions(
//   transitionType: TransitionType.slideLeft,
//   duration: const Duration(milliseconds: 150),
//   curve: Curves.easeInOut,
//   reverseCurve: Curves.fastOutSlowIn,
//   widget: PageBuyerAnnouncement(),
// ));
