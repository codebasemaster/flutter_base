import 'package:bi_smartie/https/models/auth/user_ui.dart';
import 'package:bi_smartie/utilities/sidebar_base.dart';
import 'package:flutter/material.dart';
import 'package:bi_smartie/utilities/auth/base_usage.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/services.dart';

// ignore: must_be_immutable
class BaseAuthFieldWidget extends StatefulWidget {
  BaseAuthFieldWidget(
      {Key? key,
      this.screen,
      this.label,
      this.controller,
      this.prefixicon,
      this.subfixicon,
      this.validator,
      this.enabled,
      this.iconSubfix,
      this.onChanged,
      this.hintText,
      this.keyBoardType,
      this.onFocus,
      this.maxLines,
      this.isDense,
      this.notShadow,
      this.inputFormattersCustom,
      required this.secureText,
      void Function()? setStatusIcon})
      : super(key: key);
  TextEditingController? controller;
  List<TextInputFormatter>? inputFormattersCustom;
  bool? notShadow;
  TextInputType? keyBoardType;
  void Function()? setStatusIcon;
  final Size? screen;
  bool? isDense;
  final String? label;
  final dynamic prefixicon;
  int? maxLines;
  bool? subfixicon;
  final String? Function(String?)? validator;
  final String? Function(String?)? onChanged;
  final bool? enabled;
  final IconData? iconSubfix;
  final String? hintText;
  void Function()? onFocus;
  bool secureText;
  @override
  State<BaseAuthFieldWidget> createState() => _BaseAuthFieldWidgetState();
}

class _BaseAuthFieldWidgetState extends State<BaseAuthFieldWidget> {
  late FocusNode _focusNode;

  @override
  void initState() {
    super.initState();
    _focusNode = FocusNode();
    widget.controller?.addListener(onListen);
  }

  void onListen() {
    setState(() {});
  }

  void _requestFocus() {
    setState(() {
      FocusScope.of(context).requestFocus(_focusNode);
    });
  }

  Widget inputAnyField(BuildContext context) => TextFormField(
        inputFormatters: widget.inputFormattersCustom,
        enabled: (widget.enabled == false) ? false : true,
        controller: widget.controller,
        cursorColor: Theme.of(context).primaryColor,
        focusNode: _focusNode,
        obscureText: widget.subfixicon == true || widget.subfixicon == null ? false : true,
        maxLines: widget.maxLines,
        onTap: widget.onFocus ?? _requestFocus,
        decoration: decorationInputForm(
          notShadow: widget.notShadow,
          label: widget.label?.tr(),
          focusNode: _focusNode,
          iconForm: widget.prefixicon is Widget
              ? widget.prefixicon
              : widget.prefixicon is IconData
                  ? widget.prefixicon as IconData
                  : null,
          isHidden: widget.subfixicon,
          subfix: widget.iconSubfix,
          hintText: widget.hintText,
          isDense: widget.isDense,
          setState: () {
            setState(() {
              if (widget.subfixicon != null) {
                if (widget.subfixicon == false) {
                  widget.subfixicon = true;
                } else if (widget.subfixicon == true) {
                  widget.subfixicon = false;
                }
              }
            });
          },
        ),
        validator: widget.validator,
        onChanged: widget.onChanged,
        keyboardType: widget.keyBoardType,
      );
  @override
  void dispose() {
    widget.controller?.removeListener(onListen);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => formFieldInputContainer(context, inputAnyField);
}

List<Widget> fieldUI(AuthUI data, Size sizeScreen) {
  return [
    BaseAuthFieldWidget(
      screen: sizeScreen,
      label: data.text,
      prefixicon: data.icon,
      subfixicon: data.subfix,
      iconSubfix: data.iconSubfix,
      controller: data.controller,
      secureText: data.subfix == true ? true : false,
      validator: data.validator,
      setStatusIcon: data.setStatusIcon,
    ),
    const SizedBox(
      height: 20,
    ),
  ];
}

List<Widget> subTitleLogoAndDescription(String subTitle, String description) {
  return [
    const SizedBox(
      height: 10,
    ),
    const SizedBox(
      height: 10,
    ),
    Text(
      subTitle,
      style: const TextStyle(
        fontSize: 20,
        color: Colors.black,
      ),
      textAlign: TextAlign.start,
    ),
    const SizedBox(
      height: 5,
    ),
    Text(
      description,
      style: const TextStyle(
        fontSize: 13,
        color: Colors.black,
      ),
      textAlign: TextAlign.start,
    ),
    const SizedBox(
      height: 10,
    ),
  ];
}

ElevatedButton elevateButtonAuth(void Function()? onPressed, Size sizeScreen, String subTitle) {
  return ElevatedButton(
    onPressed: onPressed,
    style: ElevatedButton.styleFrom(
      backgroundColor: colorApp,
      padding: const EdgeInsets.symmetric(
        horizontal: 30,
        vertical: 10,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      fixedSize: Size(
        sizeScreen.width.toDouble(),
        50.0,
      ),
    ),
    child: Text(
      subTitle.toUpperCase(),
      style: const TextStyle(
        fontSize: 13,
      ),
    ),
  );
}

ElevatedButton elevateButton(void Function()? onPressed,
        {required Size sizeScreen, String? title, bool isUpper = false}) =>
    elevateButtonAuth(onPressed, sizeScreen, isUpper == true ? title!.toUpperCase() : title!);
