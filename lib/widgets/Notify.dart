import 'package:bi_smartie/App.dart';
import 'package:flutter/material.dart';

Widget Notify({String? username, String? datetime, String? text}) {
  return Builder(builder: (context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 1.0, vertical: 2.0),
      padding: const EdgeInsets.all(8.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: borderRadius,
        boxShadow: const [boxShadow],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              imageRound(
                Image.asset('assets/images/logo.png'),
                borderRadius: BorderRadius.circular(20.0),
                width: 30,
                height: 30,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    height: 15.0,
                    child: Text(
                      username.toString(),
                      style: const TextStyle(fontWeight: FontWeight.w500, fontSize: 12),
                    ),
                  ),
                  SizedBox(
                    child: Text(
                      datetime.toString(),
                      style: const TextStyle(fontSize: 9),
                    ),
                  ),
                ],
              )
            ],
          ),
          Text(
            text.toString(),
            maxLines: 1,
            softWrap: true,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.start,
            style: const TextStyle(fontSize: 13),
          )
        ],
      ),
    );
  });
}
