import 'package:bi_smartie/https/repository/share_preferences.dart';
import 'package:flutter/material.dart';

class Controller extends GetItAppModel {
  String? key = '';
  dynamic data;
  Controller({this.key});

  get getData => data;

  void notify() {
    if (key != '') {
      SharePreferences.saveData(key!, data);
    }
    notifyListeners();
  }

  void offlineData() {
    if (key != '') {
      data = SharePreferences.readData(key!);
    }
    notify();
  }

  void cachedData(String key, void respone) async {
    final res = await SharePreferences.readData(key);
    if (res != null) {
      data = res;
      notifyListeners();
    }
    respone;
  }

  @override
  void fetchData() {
    // TODO: implement fetchData
  }
}

abstract class GetItAppModel extends ChangeNotifier {
  void fetchData();
}
