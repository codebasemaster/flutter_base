import 'package:flutter/material.dart';

/// Default builder method for sidebar elements building
typedef SidebarBuilder = Widget Function(BuildContext context, bool extended);
