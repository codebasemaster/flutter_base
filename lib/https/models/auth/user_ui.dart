import 'package:community_material_icon/community_material_icon.dart';
import 'package:bi_smartie/utilities/auth/base_usage.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

class AuthUI {
  late final IconData? icon;
  String text;
  bool? subfix;
  IconData? iconSubfix;
  TextEditingController? controller;
  void Function()? setStatusIcon;
  final String? Function(String?)? validator;
  bool secureText;
  TextInputType? inputType;
  AuthUI({
    this.icon,
    required this.text,
    this.subfix,
    this.iconSubfix,
    this.validator,
    this.controller,
    required this.secureText,
    this.setStatusIcon,
    this.inputType,
  });
}

String? responseValidatePhone(String? value) {
  if (value?.isEmpty ?? true) {
    return "auth.error.phone_error.require".tr();
  } else if (validatePhoneNumber(value) == false) {
    return "auth.error.phone_error.format".tr();
  }
  return null;
}

String? responseValidateUserName(String? value) {
  if (value?.isEmpty ?? true) {
    return "auth.error.username_error.require".tr();
  } else if (validatePhoneNumber(value) == true) {
    return "auth.error.username_error.format".tr();
  } else if (validateEmail(value) == true) {
    return "auth.error.username_error.format".tr();
  }
  return null;
}

List<AuthUI> formFieldRegisterNoPassword({
  TextEditingController? usernameController,
  TextEditingController? phoneController,
}) {
  final data = [
    AuthUI(
      icon: CommunityMaterialIcons.account_outline,
      text: "auth.username".tr(),
      secureText: false,
      validator: responseValidateUserName,
      controller: usernameController,
      inputType: TextInputType.text,
    ),
    AuthUI(
      icon: CommunityMaterialIcons.phone_outline,
      text: "auth.phone".tr(),
      secureText: false,
      validator: responseValidatePhone,
      controller: phoneController,
      inputType: TextInputType.phone,
    ),
  ];
  return data;
}

String? responseValidatePassword(String? value) {
  if (value?.isEmpty ?? true) {
    return "auth.error.password_error.require".tr();
  } else if (validatePassword(value) == false) {
    return "auth.error.password_error.format".tr();
  }
  return null;
}

List<AuthUI> registerFormFieldPassword({
  TextEditingController? passwordController,
  TextEditingController? confirmPasswordController,
  required bool isHiddenPass,
  required bool isHiddenCon,
  void Function()? setStateIconPass,
  void Function()? setStateIconCon,
}) {
  final data = [
    AuthUI(
        text: "auth.password".tr(),
        icon: CommunityMaterialIcons.lock_outline,
        subfix: isHiddenPass,
        secureText: isHiddenPass,
        controller: passwordController,
        setStatusIcon: setStateIconPass,
        validator: responseValidatePassword,
        inputType: TextInputType.visiblePassword),
    AuthUI(
        text: "auth.confirm_password".tr(),
        icon: CommunityMaterialIcons.lock_outline,
        subfix: isHiddenCon,
        secureText: isHiddenCon,
        controller: confirmPasswordController,
        setStatusIcon: setStateIconCon,
        inputType: TextInputType.visiblePassword,
        validator: (value) {
          if (value != passwordController?.text) {
            return "auth.error.confirm_password_error.match".tr();
          }
          return null;
        }),
  ];
  return data;
}
