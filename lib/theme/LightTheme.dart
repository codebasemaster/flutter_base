import 'package:flutter/material.dart';
import './constants.dart';

ThemeData lightTheme = ThemeData(
  fontFamily: 'Georgia',
  // brightness: Brightness.light,
  // primaryColor: COLOR_PRIMARY,
  brightness: Brightness.light,
  primaryColor: COLOR_PRIMARY_W,
  backgroundColor: COLOR_BACKGROUND_W,
  // accentColor: COLOR_ACCENT_W,
  // accentIconTheme: IconThemeData(color: COLOR_SWATCH_W),
  dividerColor: COLOR_LESS_W,

  colorScheme: ColorScheme.fromSwatch().copyWith(
    primary: COLOR_PRIMARY_D,
    secondary: COLOR_SECOND_D,
    onPrimary: COLOR_PRIMARY_W,
  ),
  switchTheme: SwitchThemeData(
    trackColor: MaterialStateProperty.all<Color>(COLOR_BUTTON_W),
    thumbColor: MaterialStateProperty.all<Color>(COLOR_ACCENT_W),
    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
  ),
  floatingActionButtonTheme: const FloatingActionButtonThemeData(
    backgroundColor: COLOR_BUTTON_W,
    foregroundColor: COLOR_SWATCH_W,
  ),
  inputDecorationTheme: InputDecorationTheme(
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(20.0),
      borderSide: BorderSide.none,
    ),
    filled: true,
    fillColor: Colors.grey.withOpacity(0.1),
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ButtonStyle(
      padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
        const EdgeInsets.symmetric(horizontal: 40.0, vertical: 20.0),
      ),
      shape: MaterialStateProperty.all<OutlinedBorder>(
        RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
      ),
      backgroundColor: MaterialStateProperty.all<Color>(COLOR_ACCENT_W),
      foregroundColor: MaterialStateProperty.all<Color>(COLOR_SECOND_W),
      overlayColor: MaterialStateProperty.all<Color>(COLOR_LESS_W),
    ),
  ),
);
