import 'package:bi_smartie/https/models/Model.dart';

class UserRole extends Model {
  UserRole({this.status, this.rid, this.uid}) : super();
  bool? status = false;
  String? rid, uid;

  factory UserRole.fromJson(Map<String, dynamic> json) => UserRole(
      rid: json['rid'] as String, uid: json['uid'] as String, status: json['status'] as bool);
  Map<String, dynamic> toJson() => <String, dynamic>{'rid': rid, 'uid': uid, 'status': status};
}
