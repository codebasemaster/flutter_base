import 'package:flutter/material.dart';
import 'package:bi_smartie/App.dart';

Widget Middleware({Key? key, String? type, required Widget child}) {
  return Builder(builder: (context) {
    if (context.watch<AuthController>().user is AuthUser) {
      final AuthUser auth = context.watch<AuthController>().user as AuthUser;
      print(auth.roles.join(","));
      final role = auth.roles[0]['rid'].toString().toLowerCase();
      if (role == 'admin') return child;
      // if (role == 'seller') return child;
      // return Column(
      //   children: [Text(auth.roles.toString()), child],
      // );
    }
    // print(auth.toJson().toString());
    return const SizedBox();
  });
}
