import 'dart:io';

import 'package:bi_smartie/App.dart';
import 'package:bi_smartie/screens/pages/page_university.dart';
import 'package:bi_smartie/widgets/auth/route_auth_animate_widget.dart';
import 'package:flutter/material.dart';

class ScreenHome extends StatefulWidget {
  const ScreenHome({Key? key}) : super(key: key);

  @override
  State<ScreenHome> createState() => _ScreenHomeState();
}

class _ScreenHomeState extends State<ScreenHome> {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();
  final ScrollController _scrollController = ScrollController();
  List collection = [];
  AuthUser? user;
  Image? imageSchool;

  late double sizeAppBar = 45;
  late double scroll = 0;
  double _scrollPosition = 0;
  _scrollListener() {
    scroll = _scrollController.position.pixels;
    setState(() {
      _scrollPosition = scroll;
    });
  }

  List<Image> promotions = <Image>[
    Image.asset('assets/images/promotion.png'),
    Image.asset('assets/images/promotion.png'),
    Image.asset('assets/images/promotion.png'),
    Image.asset('assets/images/promotion.png'),
    Image.asset('assets/images/promotion.png'),
    Image.asset('assets/images/promotion.png'),
  ];

  void loadImage() async {
    final data = await imageAssets.load('school_comp.jpg');
  }

  @override
  void initState() {
    _scrollController.addListener(_scrollListener);
    if (Platform.isAndroid || Platform.isIOS) {
      sizeAppBar = 45;
    } else {
      sizeAppBar = 10.0;
    }
    loadImage();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // if (_scrollPosition > 50) {
    //   context.read<ScreenProvider>().isBright(false);
    // } else {
    //   context.read<ScreenProvider>().isBright(true);
    // }

    return Scaffold(
      // backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Container(
        margin: const EdgeInsets.symmetric(horizontal: 14.0, vertical: 5.0),
        // padding: const EdgeInsets.only(bottom: 15),
        child: ClipRRect(
          borderRadius: const BorderRadius.all(Radius.circular(20.0)),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // SizedBox(height: MediaQuery.of(context).size.height / 8),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(18.0),
                      child: promotions[0],
                    ),
                    hspacing,
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: promotions
                          .map((e) => Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: ClipOval(
                                  child: Container(
                                      width: 5,
                                      height: 5,
                                      color: colorApp,
                                      child: const SizedBox()),
                                ),
                              ))
                          .toList(),
                    ),
                    hspacing,
                    const Text(
                      'Our Services',
                      style:
                          TextStyle(fontSize: h3, fontWeight: FontWeight.w500),
                    ),
                    hspacing,
                    const Text(
                      'See our rich services and information',
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(height: 30),
                    Row(
                      children: [
                        Expanded(
                          child: _card(
                            onPress: () => Navigator.push(
                              context,
                              CustomRouterPage(
                                direction: AxisDirection.right,
                                child: const PageUniversity(),
                              ),
                            ),
                            image: imgSchoolComp,
                            text: 'school_comp'.tr().toTitleCase(),
                          ),
                        ),
                        Expanded(
                          child: _card(
                            image: imgScholarshipComp,
                            text: 'scholarship_comp'.tr().toTitleCase(),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: _card(
                            image: imghealthComp,
                            text: 'mentalhealth_comp'.tr().toTitleCase(),
                          ),
                        ),
                        const Expanded(child: SizedBox())
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const SizedBox(),
                        TextButton(
                          onPressed: () {
                            setState(() {});
                          },
                          child: Text(
                            (true ? 'more...' : 'less').toCapitalized(),
                            textAlign: TextAlign.justify,
                            style: const TextStyle(color: Colors.indigo),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'Follow Us',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                    ),
                    boxCard(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          ClipOval(
                            child: Image.asset(
                              'assets/icons/facebook.png',
                              height: 40,
                            ),
                          ),
                          ClipOval(
                            child: Image.asset(
                              'assets/icons/telegram.png',
                              height: 40,
                            ),
                          ),
                          ClipOval(
                            child: Image.asset(
                              'assets/icons/youtube.png',
                              height: 40,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _card({dynamic image, String? text, void Function()? onPress}) {
    return Builder(builder: (BuildContext context) {
      return Column(
        children: [
          boxCard(
            onTap: onPress,
            padding: const EdgeInsets.all(0),
            margin: const EdgeInsets.all(0),
            // color: const Color.fromARGB(255, 57, 174, 241),
            borderRadiusCard: 18.0,
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(radius * 1.5)),
              child: (image is Image || image is Widget)
                  ? image
                  : Image.asset(
                      width: MediaQuery.of(context).size.width / 2,
                      image as String,
                      fit: BoxFit.cover,
                    ),
            ),
          ),
          SizedBox(
            height: 40,
            width: 200,
            child: Text(
              text!,
              textAlign: TextAlign.center,
              maxLines: 2,
              style: TextStyle(color: fontColor),
            ),
          ),
        ],
      );
    });
  }
}
