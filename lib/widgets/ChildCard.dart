import 'package:bi_smartie/App.dart';
import 'package:flutter/material.dart';

// ignore: non_constant_identifier_names
Widget childCard({
  Icon? icon,
  Widget? image,
  Color? color,
  Widget? child,
  bool? line = true,
  Size? size,
  void Function()? onPressed,
}) {
  return Builder(builder: (BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(radius * 1.5),
      child: ElevatedButton(
        onPressed: onPressed ?? () => {},
        style: ElevatedButton.styleFrom(
          foregroundColor: Theme.of(context).hintColor,
          backgroundColor: Theme.of(context).cardColor,
          shadowColor: Colors.transparent,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            if (image is Image || image is Widget)
              ClipOval(
                child: Container(
                  height: 40,
                  width: 40,
                  color: color ?? const Color.fromARGB(53, 185, 183, 183),
                  child: image,
                ),
              )
            else if (icon is Icon)
              ClipOval(
                child: Container(
                  height: 40,
                  width: 40,
                  padding: const EdgeInsets.all(6.0),
                  color: color ?? const Color.fromARGB(53, 185, 183, 183),
                  child: icon,
                ),
              )
            else
              const SizedBox(),
            (image is Image || icon is Icon)
                ? const SizedBox(width: 10)
                : const SizedBox(),
            Expanded(
              child: Container(
                height: size?.height ?? 60,
                decoration: BoxDecoration(
                  border: Border.symmetric(
                    horizontal: BorderSide(
                      width: 0.1,
                      color: (line == true)
                          ? Theme.of(context).hintColor
                          : Colors.transparent,
                    ),
                  ),
                ),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: child,
                ),
              ),
            )
          ],
        ),
      ),
    );
  });
}
// Expanded(
//   child: ClipRRect(
//     borderRadius: borderRadius,
//     child: SizedBox(
//       height: 45,
//       child: ElevatedButton(
//         style: ElevatedButton.styleFrom(
//           foregroundColor: Colors.red,
//           backgroundColor: Colors.grey.shade100,
//           shadowColor: Colors.white,
//           elevation: 2,
//         ),
//         onPressed: () => {},
//         child: Row(
//           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//           children: const [
//             Icon(CommunityMaterialIcons.phone, size: 20),
//             Text('Call')
//           ],
//         ),
//       ),
//     ),
//   ),
// )
