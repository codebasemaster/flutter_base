import 'package:injectable/injectable.dart';

@singleton // or lazySingleton
class DataSource {
  @disposeMethod
  void dispose() {
    // logic to dispose instance
  }
}

// @Singleton(dispose: disposeDataSource)
// class DataSource {
//   void dispose() {
//     // logic to dispose instance
//   }
// }

// /// dispose function signature must match Function(T instance)
// FutureOr disposeDataSource(DataSource instance) {
//   instance.dispose();
// }
