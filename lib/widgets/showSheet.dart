import 'package:bi_smartie/App.dart';
import 'package:flutter/material.dart';

void ShowSheet(context, {required Widget child, double? width, double? height, bool? isScroll}) {
  showModalBottomSheet(
    isScrollControlled: true,
    backgroundColor: Colors.transparent,
    context: context,
    builder: (context) {
      return SafeArea(
        child: Container(
          height: height,
          width: width,
          margin: const EdgeInsets.only(top: 40.0),
          padding: const EdgeInsets.all(8),
          decoration: const BoxDecoration(
              // color: Color.fromARGB(255, 230, 230, 230),
              // color: Colors.red,
              color: Color.fromARGB(255, 243, 243, 243),
              borderRadius: sheetBorder),
          child: Stack(
            children: <Widget>[
              Container(
                height: 5,
                alignment: Alignment.center,
                child: SizedBox(
                  width: 40.0,
                  height: 5.0,
                  child: Container(
                    decoration: BoxDecoration(boxShadow: const [
                      BoxShadow(
                        offset: Offset(0, 1),
                        blurRadius: 1,
                        spreadRadius: 0.2,
                        color: Color.fromARGB(200, 230, 230, 230),
                      )
                    ], borderRadius: borderRadius, color: Colors.white),
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 10.0),
                child: ClipRRect(
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(14.0),
                    topRight: Radius.circular(14.0),
                  ),
                  child: (isScroll == true)
                      ? SingleChildScrollView(
                          child: child,
                        )
                      : child,
                ),
              ),
            ],
          ),
        ),
      );
    },
  );
}
