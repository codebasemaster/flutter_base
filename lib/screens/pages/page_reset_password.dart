import 'package:bi_smartie/https/controller/auth_controller.dart';
import 'package:bi_smartie/https/models/auth/user_ui.dart';
import 'package:bi_smartie/utilities/helper.dart';
import 'package:bi_smartie/widgets/auth/base_auth_field_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

//TODO: get uid-> new password and password and press
class PageResetPassword extends StatefulWidget {
  String uid;
  PageResetPassword({Key? key, required this.uid}) : super(key: key);

  @override
  State<PageResetPassword> createState() => _PageResetPasswordState();
}

class _PageResetPasswordState extends State<PageResetPassword> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size sizeScreen = MediaQuery.of(context).size;
    const int imageBottom = 100;
    bool confirmPasswordSubfixStatus = false;
    bool passwordSubfixStatus = false;
    void Function()? setStatusPassIcon() {
      setState(() {
        if (passwordSubfixStatus == true) {
          passwordSubfixStatus = false;
        } else {
          passwordSubfixStatus = true;
        }
      });
      return null;
    }

    void Function()? setStatusConIcon() {
      setState(() {
        if (confirmPasswordSubfixStatus == true) {
          confirmPasswordSubfixStatus = false;
        } else {
          confirmPasswordSubfixStatus = true;
        }
      });
      return null;
    }

    List<Widget> widgetFieldUI = [];
    for (var data in registerFormFieldPassword(
      passwordController: passwordController,
      confirmPasswordController: confirmPasswordController,
      isHiddenCon: confirmPasswordSubfixStatus,
      isHiddenPass: passwordSubfixStatus,
      setStateIconCon: setStatusConIcon,
      setStateIconPass: setStatusPassIcon,
    )) {
      for (var element in fieldUI(data, sizeScreen)) {
        widgetFieldUI.add(element);
      }
    }
    String subTitle = "auth.reset_password.sub_title".tr();
    String description = "auth.reset_password.description".tr();
    String buttonTitle = "auth.reset_password.button_title".tr();

    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
          child: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            SizedBox(
              height: sizeScreen.height > 690 ? sizeScreen.height - imageBottom : null,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    child: IconButton(
                      focusColor: Colors.white,
                      splashRadius: 2,
                      alignment: Alignment.topLeft,
                      padding: const EdgeInsets.all(1),
                      icon: SvgPicture.asset("assets/icons/Union.svg"),
                      onPressed: () => Navigator.pop(context),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  ...subTitleLogoAndDescription(subTitle, description),
                  Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        ...widgetFieldUI,
                        elevateButtonAuth(onPressed, sizeScreen, buttonTitle)
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      )),
    );
  }

  void onPressed() {
    if (_formKey.currentState!.validate()) {
      AuthController auth = AuthController();
      waitingMessage(context);
      auth.resetPassword(widget.uid, passwordController.text).then((value) {
        showErrorOrSuccess(
          text: "auth.reset_password.success".tr(),
          context: context,
          statusFail: false,
        );
        Navigator.of(context).pushNamedAndRemoveUntil("login", (route) => false);
      }).catchError((onError) {
        showErrorOrSuccess(
          text: "auth.reset_password.fail".tr(),
          context: context,
          statusFail: true,
        );
        if (kDebugMode) {
          print(onError);
        }
      });
    }
  }
}
