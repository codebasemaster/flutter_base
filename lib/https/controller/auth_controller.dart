import 'dart:async';
import 'dart:convert';

import 'package:bi_smartie/App.dart';
import 'package:bi_smartie/https/services/auth_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthController extends Controller {
  final AuthService service = AuthService();
  dynamic location;
  AuthUser? _user;
  String? _token;
  String? _refreshToken;

  get user => _user;
  get token => _token;
  get refresh_token => _refreshToken;

  AuthController() : super(key: "AuthController");

  // Future<void> fetchData() async {
  //   data = await service.fetchAvatar();
  //   notifyListeners();
  // }

  Future<void> getUserProvider() async {
    var data = await SharePreferences.readData("user");
    if (data != null) {
      _user = AuthUser.fromGenToken(jsonDecode(data));
    }
    notifyListeners();
  }

  Future logIn(AuthUser user) async {
    final userData = await service.logIn(user);
    _user = AuthUser.fromGenToken(userData["data"]);
    _token = userData["data"]['token'];
    _refreshToken = userData["data"]['refreshToken'];
    notifyListeners();
    SharePreferences.saveData('user', _user);
    SharePreferences.saveData('token', _token);
    SharePreferences.saveData('refreshToken', _refreshToken);
    return userData;
  }

  Future register(AuthUser user) async {
    return await service.register(user);
  }

  Future generateToken(AuthUser user) async {
    return await service.generateToken(user);
  }

  Future getLocation() async {
    return await service.getLocationDevice();
  }

  Future logOut(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove("user");
    prefs.remove("token");
    prefs.remove("refreshToken");
    _user = null;
    return await service.logout(token);
  }

  Future resetPassword(String uid, String password) async {
    return await service.resetPassword(uid, password);
  }

  Future verifyCode(String? token, header) async {
    return await service.verifyToken(token, header);
  }

  Future refreshToken(String? refreshToken) async {
    return await service.getNewToken(refreshToken);
  }
}
