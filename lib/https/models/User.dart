import 'dart:convert';
import 'package:bi_smartie/https/models/Model.dart';
import 'package:bi_smartie/https/models/address.dart';

class AuthUser extends Model {
  String? token;
  String? _refreshToken;
  String? _displayName;
  String? _phoneNumber;
  String? _email;
  String? _password;
  String? _uid;
  String? _iat;
  String? _exp;
  List? _roles;
  String? _ipAddress;
  Address? _address;

  get refreshToken => _refreshToken;

  set refreshToken(value) => _refreshToken = value;

  get displayName => _displayName;

  set displayName(value) => _displayName = value;

  get phoneNumber => _phoneNumber;

  set phoneNumber(value) => _phoneNumber = value;

  get email => _email;

  set email(value) => _email = value;

  get password => _password;

  set password(value) => _password = value;

  get uid => _uid;

  set uid(value) => _uid = value;

  get iat => _iat;

  set iat(value) => _iat = value;

  get exp => _exp;

  set exp(value) => _exp = value;

  get roles => _roles;

  set roles(value) => _roles = value;

  get ipAddress => _ipAddress;

  set ipAddress(value) => _ipAddress = value;

  Address? get address => _address;

  set address(value) => _address = value;

  AuthUser();
  Future<void> Add() async {
    await Future.delayed(const Duration(seconds: 2));
    // _name = 'Goodbye';
  }

  void setRegisterUser({
    int? provinceId,
    int? districtId,
    int? villageId,
    int? communeId,
    String? displayName,
    String? phoneNumber,
    String? password,
  }) {
    _displayName = displayName;
    _phoneNumber = phoneNumber;
    _password = password;
    _address = Address(communeId, districtId, provinceId, villageId);
  }

  AuthUser.formJson(Map<String, dynamic> json) {
    uid = json['uid'];
    _displayName = json['displayName'];
    phoneNumber = json['phoneNumber'];
    email = json['email'];
    roles = json['roles'];
    ipAddress = json['ip_address'];
    _password = json['password'];
    token = json["token"];
    refreshToken = json["refreshToken"];
  }
  AuthUser.fromGenToken(Map<String, dynamic> json) {
    token = json["token"];
    refreshToken = json["refreshToken"];
    roles = json["roles"];
    displayName = json["displayName"];
    phoneNumber = json["phoneNumber"];
    address = Address.fromJson(json["address"] ?? {});
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['uid'] = uid;
    data['displayName'] = displayName;
    data['phoneNumber'] = phoneNumber;
    data['email'] = email;
    data['roles'] = roles;
    data['ip_address'] = ipAddress;
    data['password'] = password;
    return data;
  }

  Map<String, dynamic> submitRegister() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['displayName'] = displayName;
    data['phoneNumber'] = phoneNumber;
    data['password'] = password;
    data['address'] = jsonEncode(address?.toJson() ?? {});
    return data;
  }

  Map<String, dynamic> submitOTP() {
    return {"uid": uid};
  }

  Map<String, dynamic> submitLogin() {
    return {"key": displayName, "password": password};
  }

  void fromGenToken(json) {}
}

enum Role { admin, buyer, seller }
