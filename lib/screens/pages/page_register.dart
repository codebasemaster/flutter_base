import 'dart:convert';

import 'package:bi_smartie/utilities/auth/base_usage.dart';
import 'package:bi_smartie/widgets/auth/route_auth_animate_widget.dart';
import 'package:bi_smartie/https/models/auth/user_ui.dart';
import 'package:bi_smartie/utilities/auth/toast.dart';
import 'package:bi_smartie/widgets/auth/base_auth_field_widget.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:bi_smartie/App.dart';
import 'package:fluttertoast/fluttertoast.dart';

class PageRegister extends StatefulWidget {
  const PageRegister({Key? key}) : super(key: key);

  @override
  State<PageRegister> createState() => _PageRegisterState();
}

class _PageRegisterState extends State<PageRegister> {
  late FToast fToast;
  late double divide = 1.0;
  final _formKey = GlobalKey<FormState>();
  final keyController = TextEditingController();
  final passwordController = TextEditingController();
  final confirmPasswordController = TextEditingController();
  var _statusPhone = false;

  final currentPlaform = defaultTargetPlatform;
  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  @override
  void dispose() {
    super.dispose();
  }

  _showToast(int status) {
    String textStatus =
        status == 1 ? "auth.register.success".tr() : "auth.register.fail".tr();
    Widget toast = tickWidget(textStatus, status);
    fToast.showToast(
      child: toast,
      gravity: ToastGravity.BOTTOM,
      toastDuration: const Duration(seconds: 2),
    );
    // fToast.removeCustomToast();
  }

  @override
  Widget build(BuildContext context) {
    final sizeScreen = MediaQuery.of(context).size;

    final String subTitle = "auth.register.sub_title".tr();
    final bool isRequestLogin = context.watch<ScreenProvider>().isToPageLogin;
    const loginKey = "auth.login.key";
    const passwordKey = "auth.password";
    const confirmPasswordKey = "auth.confirm_password";
    final textForgetPassword = '${"auth.login.forget_password".tr()} ?';

    void onPressed() {
      context.read<ScreenProvider>().setVerifyCode(false);
      if (_formKey.currentState!.validate()) {
        if (_statusPhone == false) {
          AuthController auth = AuthController();
          waitingMessage(context);
          auth.getLocation().then((value) async {
            AuthUser user = AuthUser();
            user.displayName = keyController.text;
            user.password = passwordController.text;
            final authLogin = AuthController();

            authLogin.logIn(user).then((value) async {
              final map = jsonDecode(jsonEncode(value["data"]));
              Provider.of<AuthProvider>(context, listen: false).setUser(map);
              ScaffoldMessenger.of(context).hideCurrentSnackBar();
              RestartWidget.restartApp(context);
            }).catchError((onError) {
              final status = jsonDecode(onError)["status"];
              if (kDebugMode) {
                print(jsonDecode(onError));
              }
              if (status == 400) {
                showErrorOrSuccess(
                    text: "auth.error.email_error.match".tr(),
                    context: context,
                    statusFail: true);
              } else if (status == 403) {
                showErrorOrSuccess(
                  text: "auth.error.password_error.match".tr(),
                  context: context,
                  statusFail: true,
                );
              }
            });
          }).catchError((onError) {
            if (kDebugMode) {
              print(['catchError', onError]);
            }
          });
        } else {
          Navigator.of(context).push(
            CustomRouterPage(
              direction: AxisDirection.up,
              // child: PageVerifyCode(phoneNumber: keyController.text),
              child: const Text('hello world'),
            ),
          );
        }
      }
    }

    String? onChangeUserOrPhone(String? value) {
      if (currentPlaform != TargetPlatform.linux &&
          currentPlaform != TargetPlatform.windows) {
        if (validatePhoneNumber(value) == true) {
          setState(() {
            _statusPhone = true;
          });
        } else {
          setState(() {
            _statusPhone = false;
          });
        }
      }
      return null;
    }

    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: 25),
                    isRequestLogin
                        ? Container(
                            alignment: Alignment.topLeft,
                            child: IconButton(
                              focusColor: Colors.white,
                              splashRadius: 2,
                              highlightColor: Colors.white,
                              splashColor: Colors.white,
                              alignment: Alignment.topLeft,
                              padding: const EdgeInsets.all(1),
                              icon: const Icon(Icons.arrow_back_rounded),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                          )
                        : const SizedBox(),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 25),
                      child: ClipRRect(
                        borderRadius: borderRadius,
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Image.asset('assets/images/image_login.png'),
                            const Text(
                              'Sign Up',
                              style: TextStyle(
                                  fontSize: 24, fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 5.0),
                      child: Text(
                        'Please login to manage your preferences and access to more features.',
                        textAlign: TextAlign.center,
                      ),
                    ),
                    const SizedBox(height: 25),
                    Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          BaseAuthFieldWidget(
                            controller: keyController,
                            screen: sizeScreen,
                            prefixicon:
                                Image.asset('assets/icons/kh_language.png'),
                            label: loginKey,
                            subfixicon: null,
                            secureText: false,
                            onChanged: onChangeUserOrPhone,
                            validator: (value) {
                              if (value?.isEmpty ?? true) {
                                return "auth.error.phone_error.require".tr();
                              }
                              return null;
                            },
                          ),
                          Column(
                            children: [
                              const SizedBox(height: 20),
                              BaseAuthFieldWidget(
                                controller: passwordController,
                                screen: sizeScreen,
                                label: passwordKey,
                                // prefixicon: CommunityMaterialIcons.lock_outline,
                                subfixicon: false,
                                secureText: true,
                                keyBoardType: TextInputType.visiblePassword,
                                validator: (value) =>
                                    responseValidatePassword(value),
                                maxLines: 1,
                              ),
                              const SizedBox(height: 20),
                              BaseAuthFieldWidget(
                                controller: confirmPasswordController,
                                screen: sizeScreen,
                                label: confirmPasswordKey,
                                // prefixicon: CommunityMaterialIcons.lock_outline,
                                subfixicon: false,
                                secureText: true,
                                keyBoardType: TextInputType.visiblePassword,
                                validator: (value) =>
                                    responseValidatePassword(value),
                                maxLines: 1,
                              ),
                            ],
                          ),
                          const SizedBox(height: 10),
                          Align(
                            alignment: Alignment.center,
                            child: ExpandableText(
                              'By registering you confirm that you accept our Terms of Services and Privacy Policy',
                              expandText: 'show_more'.tr(),
                              collapseText: 'show_less'.tr(),
                              maxLines: 2,
                              linkColor: Colors.grey.shade500,
                            ),
                          ),
                          const SizedBox(height: 20),
                          elevateButtonAuth(onPressed, sizeScreen, subTitle),
                          const SizedBox(height: 20),
                          Align(
                            alignment: Alignment.centerRight,
                            child: linkInLogin(
                                context, 'Have an account? Login', "register"),
                          ),
                        ],
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Column(
                        children: [
                          const SizedBox(height: 25),
                          const Text('----- Sign Up via ------'),
                          const SizedBox(height: 25),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ClipOval(
                                child: Image.asset(
                                  'assets/icons/facebook.png',
                                  height: 40,
                                ),
                              ),
                              wspacing,
                              ClipOval(
                                child: Image.asset(
                                  'assets/icons/google.png',
                                  height: 40,
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void onPressed() {
    if (_formKey.currentState!.validate()) {
      String phone = keyController.text.replaceAll(RegExp(r"[- ]"), "");
      String cleanPhoneNumber =
          phone.replaceAll(RegExp(r"^0|\+855|\+8550"), "+855");

      final AuthUser user = AuthUser();

      final auth = AuthController();
      waitingMessage(context);

      auth.register(user).then((value) {
        ScaffoldMessenger.of(context).hideCurrentSnackBar();

        showErrorOrSuccess(
            text: "auth.register.success".tr(),
            context: context,
            statusFail: false);
        Navigator.pushNamed(context, "login");
      }).catchError(
        (onError) {
          if (kDebugMode) {
            print(onError);
          }
          ScaffoldMessenger.of(context).hideCurrentSnackBar();
          showErrorOrSuccess(
              text: "auth.register.fail".tr(),
              context: context,
              statusFail: true);
        },
      );
      //success
    }
  }
}
