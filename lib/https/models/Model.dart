import 'package:get_it/get_it.dart';

GetIt getIt = GetIt.instance; //GetIt.I;

class Model {
  DateTime? _createdAt;
  DateTime? _updatedAt;
  get createdAt => _createdAt;

  set createdAt(value) => _createdAt = value;

  get updatedAt => _updatedAt;

  set updatedAt(value) => _updatedAt = value;

  late dynamic _model;

  /// An unmodifiable view of the items in the cart.
  get model => _model;

  // Model.formData();
}

abstract class BaseModel {
  Map<String, dynamic> fetchData();
}
