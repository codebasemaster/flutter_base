import 'package:bi_smartie/App.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

Widget imageNetwork(String url, {BoxFit? fit, double? width, double? height}) {
  return CachedNetworkImage(
    imageUrl: url,
    width: width,
    height: height,
    imageBuilder: (context, imageProvider) => Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: imageProvider,
          fit: fit ?? BoxFit.cover,
          // colorFilter: const ColorFilter.mode(Colors.transparent, BlendMode.colorBurn),
        ),
      ),
    ),
    placeholder: (context, url) => const SizedBox(
      width: 30,
      child: FittedBox(fit: BoxFit.fitWidth, child: CircularProgressIndicator(color: colorApp)),
    ),
    errorWidget: (context, url, error) => const Icon(Icons.error),
  );
}

Widget imageNetworkApp(String url, {Key? key, BoxFit? fit, double? width, double? height}) {
  late String urls = "${ENV.apiURL}/$url";
  return CachedNetworkImage(
    key: key,
    imageUrl: urls,
    width: width,
    height: height,
    imageBuilder: (context, imageProvider) => Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: imageProvider,
          fit: fit ?? BoxFit.cover,
          // colorFilter: const ColorFilter.mode(Colors.transparent, BlendMode.colorBurn),
        ),
      ),
    ),
    placeholder: (context, url) => const CircularProgressIndicator(color: colorApp),
    errorWidget: (context, url, error) => const Icon(Icons.error),
  );
}
