final typesData = {
  "km_KH": {
    "choose_type_weight": [
      {"key": "kg", "value": "គីឡូក្រាម", "title": "ទម្ងន់ស្រូវគិតជាគីឡូក្រាម"},
      {"key": "ton", "value": "តោន", "title": "ទម្ងន់ស្រូវគិតជាតោន"}
    ],
    "choose_type_money": [
      {"key": "KHR", "value": "រៀល", "title": "រូបិយប័ណ្ណជាលុយខ្មែរ"},
      {"key": "USD", "value": "ដុល្លារ", "title": "រូបិយប័ណ្ណជាលុយអាមេរិច"}
    ],
  }
};
