import 'dart:convert';
import 'package:bi_smartie/https/repository/ENV.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

class Service {
  // final Function()? service;

  String? url = ENV.apiURL;

  Service({this.url});

  Future fetchData() {
    return http.get(Uri.parse(url!));
  }

  Future get({String? uri, Map<String, dynamic>? headers}) async {
    late String urls = url! + uri!;
    final response = headers != null
        ? await http.get(Uri.parse(urls), headers: {...headers})
        : await http.get(Uri.parse(urls));
    if (response.statusCode == 200 || response.statusCode == 201) {
      return jsonDecode(response.body);
    } else {
      throw Exception("Failed to load method get");
    }
  }

  Future post({required String uri, Object? body, Map<String, String>? header}) async {
    final response = await http
        .post(Uri.parse(url! + uri), body: body, headers: header)
        .timeout(const Duration(seconds: 300));
    int status = response.statusCode;
    if (status == 200 || status == 201) {
      try {
        return jsonDecode(response.body);
      } catch (e) {
        // if (kDebugMode) {
        //   print(e);
        // }
        return response.body;
      }
      // return jsonDecode(response.body);
    } else {
      if (kDebugMode) {
        print(response.body);
        print(status);
      }
      throw response.body;
    }
  }

  Future delete(String uri, {Object? body}) async {
    final response = await http.delete(Uri.parse(url! + uri), body: body!);
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      throw Exception('Failed to load method delete');
    }
  }

  Future update(String uri, Map<String, String>? params, {Object? body, Encoding? encoding}) async {
    final response = await http.put(Uri.parse(url! + uri), headers: params, body: body!);
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      throw Exception('Failed to load method update');
    }
  }

  void create() {}
}
