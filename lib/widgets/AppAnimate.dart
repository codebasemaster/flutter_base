import 'dart:math';
import 'package:bi_smartie/widgets/MeasureSize.dart';
import 'package:flutter/widgets.dart';

Widget appAnimate({required double opacity, required Widget child, int? duration}) {
  late int timeOverlay = 500;
  return AnimatedOpacity(
    opacity: opacity,
    duration: Duration(milliseconds: timeOverlay),
    child: AnimatedContainer(
      alignment: Alignment.topCenter,
      height: opacity,
      duration: Duration(milliseconds: timeOverlay),
      child: child,
    ),
  );
}

class ShakeCurve extends Curve {
  @override
  double transform(double t) => sin(t * pi * 2);
}

class AppAnimate2 extends StatefulWidget {
  final bool? control;
  final Widget child;
  const AppAnimate2({Key? key, this.control, required this.child}) : super(key: key);

  @override
  _AppAnimateState createState() => _AppAnimateState();
}

class _AppAnimateState extends State<AppAnimate2> {
  late int timeOverlay = 500;
  Size sizeBox = Size.zero;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.control == false) {
      setState(() {
        sizeBox = Size.zero;
      });
    }
    return MeasureSize(
      onChange: (size) {
        print(size);
        setState(() {
          sizeBox = size;
        });
      },
      child: AnimatedContainer(
        height: sizeBox.height,
        duration: Duration(milliseconds: timeOverlay),
        child: widget.child,
      ),
    );
  }
}
