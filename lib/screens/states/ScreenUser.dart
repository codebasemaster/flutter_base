import 'dart:io';
import 'dart:math' as math;

import 'package:bi_smartie/App.dart';
import 'package:bi_smartie/injection.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
// import 'package:flutter/services.dart';

class ScreenUser extends StatefulWidget {
  const ScreenUser({Key? key}) : super(key: key);

  @override
  State<ScreenUser> createState() => _ScreenUserState();
}

class _ScreenUserState extends State<ScreenUser> {
  final ScrollController _scrollController = ScrollController();
  final ImagePicker imagePicker = ImagePicker();
  final Color greyColor = const Color.fromARGB(255, 189, 189, 189);
  double _scrollPosition = 0;
  List<XFile>? imageFileList = [];
  File? imageFile;
  late List<Widget> allCollection = [];
  late List<Widget> collection = [];

  late double sizeAppBar = 45.0;

  bool isExpendVerify = false;
  bool _visitSignOut = false;

  late Brightness iconColor = Brightness.light;

  AuthUser? user;

  // final Image back = Image.asset('assets/icons/back_arrow.svg', width: 25, height: 25);

  late double rotate = math.pi / 180;

  _scrollListener() {
    setState(() {
      _scrollPosition = _scrollController.position.pixels;
      _visitSignOut = _scrollController.position.userScrollDirection ==
              ScrollDirection.reverse
          ? false
          : true;
    });
    // print(_scrollController.position.userScrollDirection);
  }

  @override
  void initState() {
    _scrollController.addListener(_scrollListener);
    iconColor = Brightness.dark;

    if (Platform.isAndroid || Platform.isIOS) {
      sizeAppBar = 45;
    } else {
      sizeAppBar = 10.0;
    }
    super.initState();
  }

  void signOut(context) {
    final String? token = user?.token;
    locator.get<AuthController>().logOut(token.toString());
  }

  Widget signOutBtn(bool show, context) {
    return Visibility(
      visible: show == false ? false : true,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 4.0),
        child: elevateButton(() async {
          await SharePreferences.deleteData('isUserLogin');
          RestartWidget.restartApp(context);
        }, sizeScreen: MediaQuery.of(context).size, title: 'Sign Out'),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    user = context.watch<AuthProvider>().user;
    final sizeScreen = MediaQuery.of(context).size;

    if (Platform.isAndroid || Platform.isIOS) {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarIconBrightness: iconColor,
      ));
    }

    return Scaffold(
      body: Container(
        margin: padding,
        child: SingleChildScrollView(
          controller: _scrollController,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              hspacing,
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0, left: 8.0),
                child: Text(
                  'Account Information',
                  textAlign: TextAlign.left,
                  style: TextStyle(color: fontColor),
                ),
              ),
              boxCard(
                color: Colors.transparent,
                child: Column(
                  children: [
                    childCard(
                      icon: const Icon(Icons.person_rounded, color: colorApp),
                      child: const Text('Profile'),
                    ),
                    childCard(
                      icon: const Icon(Icons.key_off_outlined, color: colorApp),
                      child: const Text('Change Password'),
                    )
                  ],
                ),
              ),
              // hspacing,
              // Padding(
              //   padding: const EdgeInsets.only(bottom: 8.0, left: 8.0),
              //   child: Text(
              //     'My Subscription',
              //     textAlign: TextAlign.left,
              //     style: TextStyle(color: fontColor),
              //   ),
              // ),
              // boxCard(
              //   color: Colors.transparent,
              //   child: Column(
              //     children: [
              //       childCard(
              //         child: const Text('Requested Services'),
              //       ),
              //       childCard(
              //         child: const Text('Subscribed Forum'),
              //       )
              //     ],
              //   ),
              // ),
              hspacing,
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0, left: 8.0),
                child: Text(
                  'Setting and Privacy',
                  textAlign: TextAlign.left,
                  style: TextStyle(color: fontColor),
                ),
              ),
              boxCard(
                color: Colors.transparent,
                child: Column(
                  children: [
                    childCard(
                      icon: const Icon(Icons.language_rounded, color: colorApp),
                      child: const Text('Language'),
                    ),
                    childCard(
                      icon: const Icon(Icons.settings_input_component,
                          color: colorApp),
                      child: const Text('Terms of Services'),
                    ),
                    childCard(
                      icon: const Icon(Icons.privacy_tip, color: colorApp),
                      child: const Text('Privacy Policies'),
                    )
                  ],
                ),
              ),
              hspacing,
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0, left: 8.0),
                child: Text(
                  'Support & Request',
                  textAlign: TextAlign.left,
                  style: TextStyle(color: fontColor),
                ),
              ),
              boxCard(
                color: Colors.transparent,
                child: Column(
                  children: [
                    childCard(
                      icon: const Icon(Icons.info_rounded, color: colorApp),
                      child: const Text('About Us'),
                    ),
                    childCard(
                      icon: const Icon(Icons.contact_mail, color: colorApp),
                      child: const Text('Contact Us'),
                    ),
                    childCard(
                      icon: const Icon(Icons.art_track_sharp, color: colorApp),
                      child: const Text('Send Request'),
                    ),
                    childCard(
                      icon: const Icon(Icons.question_answer, color: colorApp),
                      child: const Text('Send Your Feedback'),
                    ),
                  ],
                ),
              ),
              // hspacing,
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 4.0),
                child: signOutBtn(true, context),
              ),
            ],
          ),
        ),
      ),
      // floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      // floatingActionButton: Padding(
      //   padding: const EdgeInsets.symmetric(horizontal: 18.0),
      //   child: signOutBtn(_visitSignOut, context),
      // ),
    );
  }
}
