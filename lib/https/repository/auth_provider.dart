import 'dart:convert';

import 'package:bi_smartie/https/models/User.dart';
import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthProvider extends ChangeNotifier {
  String? _smsCode;
  AuthUser? _user;
  get smsCode => _smsCode;
  set setSMSCode(String code) {
    _smsCode = code;
    notifyListeners();
  }

  AuthUser? get user => _user;
  void printLongString(String text) {
    final RegExp pattern = RegExp('.{1,800}'); // 800 is the size of each chunk
    pattern.allMatches(text).forEach((RegExpMatch match) => print(match.group(0)));
  }

  void setUserPro(value) {
    _user = value;
    notifyListeners();
  }

  void setPrefs(value) {
    SharedPreferences.getInstance().then((val) {
      val.setString("user", value);
    });
  }

  Future<void> getUser() async {
    final getShareProvider = await getUserProvider();
    _user = AuthUser.fromGenToken(getShareProvider);
    notifyListeners();
  }

  void setUser(Map<String, dynamic> value) async {
    printLongString(jsonEncode(value));
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("user", jsonEncode(value));
    _user = AuthUser.fromGenToken(value);
    notifyListeners();
  }

  Future getUserProvider() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final userLocal = prefs.getString("user");
    if (userLocal != null) {
      _user = AuthUser.fromGenToken(jsonDecode(userLocal));
    }
    notifyListeners();
  }
}
