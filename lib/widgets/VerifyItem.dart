import 'package:bi_smartie/App.dart';
import 'package:flutter/material.dart';

Widget VerifyItem({String? username, String? datetime, String? text}) {
  return Builder(builder: (context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 1.0, vertical: 2.0),
      decoration: const BoxDecoration(boxShadow: [boxShadow], color: Colors.white),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 8.0),
                child: CircleAvatar(
                  backgroundColor: Color.fromARGB(255, 237, 237, 237),
                  child: Icon(Icons.person_rounded),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    height: 15.0,
                    child: Text(
                      username.toString(),
                      style: const TextStyle(fontWeight: FontWeight.w500, fontSize: 12),
                    ),
                  ),
                  SizedBox(
                    child: Text(
                      datetime.toString(),
                      style: const TextStyle(fontSize: 9),
                    ),
                  ),
                ],
              ),
            ],
          ),
          Row(
            children: [
              ClipRRect(
                borderRadius: borderRadius,
                child: IconButton(
                    splashRadius: 25,
                    iconSize: 16,
                    onPressed: () => {},
                    icon: const Icon(Icons.person_remove_rounded,
                        color: Color.fromARGB(255, 208, 41, 41))),
              ),
              ClipRRect(
                borderRadius: borderRadius,
                child: IconButton(
                  splashRadius: 25,
                  iconSize: 16,
                  onPressed: () => {},
                  icon: const Icon(Icons.person_add_alt_rounded, color: Colors.black87),
                ),
              )
            ],
          )
        ],
      ),
    );
  });
}
