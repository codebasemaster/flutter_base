import 'dart:io';

import 'package:bi_smartie/App.dart';
import 'package:flutter/material.dart';

class ScreenNews extends StatefulWidget {
  const ScreenNews({Key? key}) : super(key: key);

  @override
  State<ScreenNews> createState() => _ScreenNewsState();
}

class _ScreenNewsState extends State<ScreenNews> {
  late double sizeAppBar = 45;
  @override
  void initState() {
    if (Platform.isAndroid || Platform.isIOS) {
      sizeAppBar = 45;
    } else {
      sizeAppBar = 10.0;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Container(
        margin: const EdgeInsets.symmetric(horizontal: 14.0, vertical: 5.0),
        child: ClipRRect(
          borderRadius: const BorderRadius.all(Radius.circular(20.0)),
          child: Center(
            child: Column(
              children: [
                hspacing,
                Padding(
                  padding: const EdgeInsets.only(bottom: 10.0, top: 30.0),
                  child: Text(
                    'BiSmartie NEWs'.toTitleCase(),
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      color: colorApp,
                    ),
                  ),
                ),
                hspacing,
                ExpandableText(
                  'Exercise your brain with BiSmartie in daily basis to improve brain function and balance your life.',
                  expandText: 'show_more'.tr(),
                  collapseText: 'show_less'.tr(),
                  maxLines: 2,
                  linkColor: Colors.grey.shade500,
                  textAlign: TextAlign.center,
                ),
                hspacing,
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0, vertical: 10),
                  child: boxCard(
                    padding: const EdgeInsets.all(0),
                    margin: const EdgeInsets.all(0),
                    borderRadiusCard: 18.0,
                    child: imgNewsComp,
                  ),
                ),
                const Expanded(child: Center(child: Text('Comming soon.....')))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
